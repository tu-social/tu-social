fmt: fmt-jsonnet fmt-yaml

fmt-jsonnet:
	find . -name 'vendor' -prune \
		-o -name '*.libsonnet' -print \
		-o -name '*.jsonnet' -print \
		-exec jsonnetfmt -i {} \;

check-jsonnet: fmt-jsonnet
	if git diff --exit-code; then \
		echo 'Everything is perfectly formatted, good job!'; \
	else \
		echo '[ERROR] The following files are not properly formatted:'; \
		git status --porcelain; \
		echo 'You can format the jsonnet locally with:'; \
		echo '$ make fmt-jsonnet'; \
	fi

install-yamlfmt:
	go install github.com/devopyio/yamlfmt@latest

fmt-yaml:
	find . -name 'vendor' -prune \
		-o -name 'charts' -prune \
		-o -name 'chartfile.yaml' -prune \
		-o -name '*.yml' -print \
		-o -name '*.yaml' -print \
		-exec yamlfmt -f {} \;

