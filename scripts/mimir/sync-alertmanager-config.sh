#!/usr/bin/env bash

set -euo pipefail
export SHELLOPTS
umask 0077

# mimirtool: https://github.com/grafana/mimir/releases
command -v mimirtool >/dev/null 2>&1 || { echo "mimirtool is not installed, please install it from"; exit 1; }
command -v yq >/dev/null 2>&1 || { echo "yq is not installed, please install it from"; exit 1; }
command -v jsonnet >/dev/null 2>&1 || { echo "jsonnet is not installed, please install it from"; exit 1; }

dirname0="$(dirname "$0")"
mimir_dir="$(cd "$dirname0/../../jsonnet/environments/mimir" && pwd -P)"

DRY_RUN="${DRY_RUN:-false}"
while [[ $# -gt 0 ]]; do
  case "$1" in
  --dry-run)
    DRY_RUN="true"
    shift
    ;;
  *)
    break
    ;;
  esac
done

if [[ "$DRY_RUN" == "false" ]]; then
  MIMIR_USER="${MIMIR_USER:-default}"
  MIMIR_PASS="${MIMIR_PASS:-$(cat "$mimir_dir/secrets/mimir-admin.default.secret.txt")}"
  MIMIR_ADDR="${MIMIR_ADDR:-https://$MIMIR_USER:$MIMIR_PASS@mimir-admin.tu.social}"
fi

alertmanager_config="$(mktemp alertmanager.default.config.yaml.XXXXXX)"
trap 'shred -u "$alertmanager_config"' EXIT

echo "Rendering config"
jsonnet --ext-str "DRY_RUN=$DRY_RUN" "$mimir_dir/alertmanager.default.jsonnet" | yq --prettyPrint > "$alertmanager_config"
echo "OK: rendered $(wc -l "$alertmanager_config" | cut -d' ' -f1) lines of YAML"

echo "Verifying config"
mimirtool alertmanager verify "$alertmanager_config"
echo "OK"

if [[ "$DRY_RUN" == "true" ]]; then
  echo "Dry-run, not synchronizing to Mimir."
else
  echo "Loading config"
  mimirtool alertmanager load --address="$MIMIR_ADDR" --id="$MIMIR_USER" "$alertmanager_config"
  echo "OK"
fi