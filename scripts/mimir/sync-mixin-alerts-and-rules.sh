#!/usr/bin/env bash

set -euo pipefail
export SHELLOPTS
umask 0077

# mixtool: https://github.com/monitoring-mixins/mixtool
command -v mixtool >/dev/null 2>&1 || { echo "mixtool is not installed, please install it from"; exit 1; }
# mimirtool: https://github.com/grafana/mimir/releases
command -v mimirtool >/dev/null 2>&1 || { echo "mimirtool is not installed, please install it from"; exit 1; }

dirname0="$(dirname "$0")"
mimir_dir="$(cd "$dirname0/../../jsonnet/environments/mimir" && pwd -P)"
mixins_dir="$mimir_dir/mixins"

DRY_RUN="${DRY_RUN:-false}"
while [[ $# -gt 0 ]]; do
  case "$1" in
  --dry-run)
    DRY_RUN="true"
    shift
    ;;
  *)
    break
    ;;
  esac
done

if [[ "$DRY_RUN" == "false" ]]; then
  MIMIR_USER="${MIMIR_USER:-default}"
  MIMIR_PASS="${MIMIR_PASS:-$(cat "$mimir_dir/secrets/mimir-admin.default.secret.txt")}"
  MIMIR_ADDR="${MIMIR_ADDR:-https://$MIMIR_USER:$MIMIR_PASS@mimir-admin.tu.social}"
fi

build_dir="$mixins_dir/build"
mkdir -p "$build_dir"

find "$mixins_dir" -name '*.jsonnet' -print0 | while IFS= read -r -d '' mixin; do
  jsonnetfile="$(basename "$mixin")"
  # name is jsonnetfile without .jsonnet suffix
  name="${jsonnetfile%.jsonnet}"
  echo " Processing mixin from $jsonnetfile"

  mixtool generate rules "$mixin" > "$build_dir/${name}_rules.yaml"
  mixtool generate alerts "$mixin" > "$build_dir/${name}_alerts.yaml"
done


mimirtool rules check --rule-dirs "$build_dir"
if [[ "$DRY_RUN" == "true" ]]; then
  echo "Dry-run, not synchronizing to Mimir."
else
  echo "Synchronizing to Mimir"
  mimirtool rules sync --address="$MIMIR_ADDR" --id="$MIMIR_USER" --rule-dirs "$build_dir"
fi