# Tu Social

This repository contains the code required to deploy the infrastructure of tu.social website running a Mastodon instance.

## Secrets

Secrets should be gitcrypt-encrypted here, and if I left something public, please ping me.
