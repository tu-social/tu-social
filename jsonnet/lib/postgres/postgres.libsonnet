/*
Inspired by https://github.com/scriptcamp/kubernetes-postgresql
*/
local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      configMap = k.core.v1.configMap,
      container = k.core.v1.container,
      deployment = k.apps.v1.deployment,
      envVar = k.core.v1.envVar,
      pvc = k.core.v1.persistentVolumeClaim,
      secret = k.core.v1.secret,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      statefulSet = k.apps.v1.statefulSet,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

{
  _images+:: (import 'images.libsonnet'),

  _config+:: {
    pgpool_replicas: 2,
    postgres_replicas: 2,
    assert $._config.postgres_replicas <= 2 : "This bitnami-based postgres image with rempgr doesn't work fine with multiple replicas, you'll end up with multiple primaries sooner than later",

    // Should have the following fields:
    // postgresql_password
    // repmgr_password
    // pgpool_admin_password
    // backup_password
    postgres_secret_name: error 'should specify postgres secret name',
  },

  postgres_env:: [
    envVar.fromFieldPath('THIS_POD_NAME', 'metadata.name'),
    envVar.new('BITNAMI_DEBUG', 'true'),
    envVar.new('POSTGRESQL_VOLUME_DIR', '/bitnami/postgresql'),
    envVar.new('PGDATA', '/bitnami/postgresql/data'),
    envVar.new('POSTGRES_USER', 'postgres'),
    envVar.fromSecretRef('POSTGRES_PASSWORD', $._config.postgres_secret_name, 'postgresql_password'),
    envVar.new('POSTGRES_DB', 'postgres'),
    envVar.new('POSTGRESQL_LOG_HOSTNAME', 'true'),
    envVar.new('POSTGRESQL_LOG_CONNECTIONS', 'false'),
    envVar.new('POSTGRESQL_LOG_DISCONNECTIONS', 'false'),
    envVar.new('POSTGRESQL_PGAUDIT_LOG_CATALOG', 'off'),
    envVar.new('POSTGRESQL_CLIENT_MIN_MESSAGES', 'error'),
    envVar.new('POSTGRESQL_SHARED_PRELOAD_LIBRARIES', 'pgaudit, repmgr'),
    envVar.new('POSTGRESQL_ENABLE_TLS', 'no'),
    envVar.new('REPMGR_UPGRADE_EXTENSION', 'no'),
    envVar.new('REPMGR_PGHBA_TRUST_ALL', 'no'),
    envVar.new('REPMGR_MOUNTED_CONF_DIR', '/bitnami/repmgr/conf'),
    envVar.fromFieldPath('REPMGR_NAMESPACE', 'metadata.namespace'),
    envVar.new('REPMGR_PARTNER_NODES', std.join(',', [
      'postgres-%d.postgres-headless.$(REPMGR_NAMESPACE).svc.cluster.local' % id
      for id in std.range(0, $._config.postgres_replicas - 1)
    ])),
    envVar.new('REPMGR_PRIMARY_HOST', 'postgres-0.postgres-headless.$(REPMGR_NAMESPACE).svc.cluster.local'),
    envVar.new('REPMGR_NODE_NAME', '$(THIS_POD_NAME)'),
    envVar.new('REPMGR_NODE_NETWORK_NAME', '$(THIS_POD_NAME).postgres-headless.$(REPMGR_NAMESPACE).svc.cluster.local'),
    envVar.new('REPMGR_LOG_LEVEL', 'NOTICE'),
    envVar.new('REPMGR_CONNECT_TIMEOUT', '5'),
    envVar.new('REPMGR_RECONNECT_ATTEMPTS', '3'),
    envVar.new('REPMGR_RECONNECT_INTERVAL', '5'),
    envVar.new('REPMGR_USERNAME', 'repmgr'),
    envVar.fromSecretRef('REPMGR_PASSWORD', $._config.postgres_secret_name, 'repmgr_password'),
    envVar.new('REPMGR_DATABASE', 'repmgr'),
  ],

  local probeCommand = [
    'bash',
    '-ec',
    'PGPASSWORD=$POSTGRES_PASSWORD psql -w -U "postgres" -d "postgres"  -h 127.0.0.1 -c "SELECT 1"',
  ],

  postgres_scripts_configmap: configMap.new('postgres-scripts', {
    'pre-stop.sh': importstr 'postgres-files/pre-stop.sh',
  }),

  postgres_container::
    container.new('postgresql', $._images.postgres)
    + container.withPorts([k.core.v1.containerPort.new('postgresql', 5432)])
    + container.mixin.lifecycle.preStop.exec.withCommand(['/scripts/pre-stop.sh', '15'])
    + container.withImagePullPolicy('Always')
    + container.securityContext.withRunAsUser(1001)
    + container.withEnvMixin($.postgres_env)
    + container.withVolumeMounts([
      volumeMount.new('postgres-data', '/bitnami/postgresql'),
    ])
    + container.mixin.livenessProbe.exec.withCommand(probeCommand)
    + container.mixin.livenessProbe.withInitialDelaySeconds(30)
    + container.mixin.livenessProbe.withPeriodSeconds(10)
    + container.mixin.livenessProbe.withTimeoutSeconds(5)
    + container.mixin.livenessProbe.withSuccessThreshold(1)
    + container.mixin.livenessProbe.withFailureThreshold(6)
    + container.mixin.readinessProbe.exec.withCommand(probeCommand)
    + container.mixin.readinessProbe.withInitialDelaySeconds(5)
    + container.mixin.readinessProbe.withPeriodSeconds(10)
    + container.mixin.readinessProbe.withTimeoutSeconds(5)
    + container.mixin.readinessProbe.withSuccessThreshold(1)
    + container.mixin.readinessProbe.withFailureThreshold(6)
    + container.mixin.resources.withRequestsMixin({ cpu: '100m', memory: '512Mi' })
    + container.mixin.resources.withLimitsMixin({ cpu: '1', memory: '2Gi' }),

  postgres_exporter_env:: [
    envVar.new('DATA_SOURCE_URI', 'localhost?sslmode=disable'),
    envVar.new('DATA_SOURCE_USER', 'postgres'),
    envVar.fromSecretRef('DATA_SOURCE_PASS', $._config.postgres_secret_name, 'postgresql_password'),
  ],

  postgres_exporter_container::
    container.new('postgres-exporter', $._images.postgres_exporter)
    + container.withEnvMixin($.postgres_exporter_env)
    + container.withPorts([k.core.v1.containerPort.new('http-metrics', 9187)])
    + k.util.resourcesRequests('25m', '128Mi'),

  postgres_pvc::
    pvc.new()
    + pvc.mixin.metadata.withName('postgres-data')
    + pvc.mixin.spec.resources.withRequests({ storage: '4Gi' })
    + pvc.mixin.spec.withAccessModes(['ReadWriteOnce'])
    + pvc.mixin.spec.withStorageClassName('local-path'),

  postgres_statefulset:
    statefulSet.new('postgres', $._config.postgres_replicas, [$.postgres_container, $.postgres_exporter_container], [$.postgres_pvc])
    + statefulSet.mixin.spec.withServiceName('postgres-headless')
    + statefulSet.mixin.metadata.withLabels({ name: 'postgres' })
    + statefulSet.mixin.spec.template.metadata.withLabelsMixin({ app: 'postgres' })
    + statefulSet.mixin.spec.selector.withMatchLabels({ app: 'postgres' })
    + statefulSet.mixin.spec.template.spec.securityContext.withFsGroup(1001)
    + statefulSet.configMapVolumeMount($.postgres_scripts_configmap, '/scripts', {}, volume.configMap.withDefaultMode(std.parseOctal('0755'))),

  // TEST: make sure that the template for posgres pods includes a non-empty name label
  // It is necessary for prometheus scraping:
  // https://github.com/grafana/jsonnet-libs/blob/5fb2525651cc6e5100e081b10ad9fbe7e3595231/prometheus/scrape_configs.libsonnet#L32-L37
  assert $.postgres_statefulset.spec.template.metadata.labels.name == 'postgres',

  postgres_service: service.new(
    'postgres-headless',
    { app: 'postgres' },
    [servicePort.newNamed('postgresql', 5432, 'postgresql') + servicePort.withProtocol('TCP')],
  ) + service.mixin.spec.withClusterIp('None'),

  pgpool_env+:: [
    envVar.new('BITNAMI_DEBUG', 'false',),
    envVar.new('PGPOOL_BACKEND_NODES', std.join(',', [
      '%(id)d:postgres-%(id)d.postgres-headless:5432' % { id: id }
      for id in std.range(0, $._config.postgres_replicas - 1)
    ])),
    envVar.new('PGPOOL_SR_CHECK_USER', 'repmgr'),
    envVar.fromSecretRef('POSTGRES_PASSWORD', $._config.postgres_secret_name, 'postgresql_password'),
    envVar.fromSecretRef('PGPOOL_SR_CHECK_PASSWORD', $._config.postgres_secret_name, 'repmgr_password'),
    envVar.new('PGPOOL_SR_CHECK_DATABASE', 'postgres'),
    envVar.new('PGPOOL_ENABLE_LDAP', 'no'),
    envVar.new('PGPOOL_POSTGRES_USERNAME', 'postgres'),
    envVar.fromSecretRef('PGPOOL_POSTGRES_PASSWORD', $._config.postgres_secret_name, 'postgresql_password'),
    envVar.new('PGPOOL_ADMIN_USERNAME', 'admin'),
    envVar.fromSecretRef('PGPOOL_ADMIN_PASSWORD', $._config.postgres_secret_name, 'pgpool_admin_password'),
    envVar.new('PGPOOL_ENABLE_LOAD_BALANCING', 'yes'),
    envVar.new('PGPOOL_ENABLE_LOG_CONNECTIONS', 'no'),
    envVar.new('PGPOOL_ENABLE_LOG_HOSTNAME', 'yes'),
    envVar.new('PGPOOL_ENABLE_LOG_PER_NODE_STATEMENT', 'no'),
    envVar.new('PGPOOL_CHILD_LIFE_TIME', ''),
    envVar.new('PGPOOL_ENABLE_TLS', 'no'),
  ],

  pgpool_container::
    container.new('pgpool', $._images.pgpool)
    + container.withImagePullPolicy('Always')
    + container.securityContext.withRunAsUser(1001)
    + container.withEnvMixin($.pgpool_env)
    + container.withPorts([k.core.v1.containerPort.new('postgresql', 5432)])
    + container.mixin.livenessProbe.exec.withCommand(['/opt/bitnami/scripts/pgpool/healthcheck.sh'])
    + container.mixin.livenessProbe.withInitialDelaySeconds(30)
    + container.mixin.livenessProbe.withPeriodSeconds(10)
    + container.mixin.livenessProbe.withTimeoutSeconds(5)
    + container.mixin.livenessProbe.withSuccessThreshold(1)
    + container.mixin.livenessProbe.withFailureThreshold(5)
    + container.mixin.readinessProbe.exec.withCommand(['bash', '-ec', 'PGPASSWORD=${PGPOOL_POSTGRES_PASSWORD} psql -U "postgres" -d "postgres" -h /opt/bitnami/pgpool/tmp -tA -c "SELECT 1" >/dev/null'])
    + container.mixin.readinessProbe.withInitialDelaySeconds(5)
    + container.mixin.readinessProbe.withPeriodSeconds(5)
    + container.mixin.readinessProbe.withTimeoutSeconds(5)
    + container.mixin.readinessProbe.withSuccessThreshold(1)
    + container.mixin.readinessProbe.withFailureThreshold(5)
    + container.mixin.resources.withRequestsMixin({ cpu: '100m', memory: '512Mi' })
    + container.mixin.resources.withLimitsMixin({ cpu: '1', memory: '2Gi' }),

  pgpool_deployment:
    deployment.new('pgpool', $._config.pgpool_replicas, $.pgpool_container)
    + deployment.mixin.spec.template.metadata.withLabels({ app: 'pgpool' })
    + deployment.mixin.spec.selector.withMatchLabels({ app: 'pgpool' })
    + deployment.mixin.spec.template.spec.securityContext.withFsGroup(1001),

  pgpool_service: service.new(
    'pgpool',
    { app: 'pgpool' },
    [servicePort.newNamed('pgpool', 5432, 'postgresql') + servicePort.withProtocol('TCP')],
  ),
}
