{
  postgres: 'docker.io/bitnami/postgresql-repmgr:14-debian-11',
  postgres_exporter: 'quay.io/prometheuscommunity/postgres-exporter:v0.11.1',
  pgpool: 'docker.io/bitnami/pgpool:4',
  aws_cli: 'amazon/aws-cli',
}
