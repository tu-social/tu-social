local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      cronJob = k.batch.v1.cronJob,
      configMap = k.core.v1.configMap,
      container = k.core.v1.container,
      deployment = k.apps.v1.deployment,
      envVar = k.core.v1.envVar,
      pvc = k.core.v1.persistentVolumeClaim,
      secret = k.core.v1.secret,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      statefulSet = k.apps.v1.statefulSet,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

{
  _images+:: (import 'images.libsonnet'),

  _config+:: {
    // Postgres credentials
    postgres_backup_postgres_username: 'postgres',
    postgres_backup_postgres_password_secret_name: error 'should specify postgres backup postgres password secret name',
    postgres_backup_postgres_password_secret_key: error 'should specify postgres backup postgres password secret key',
    postgres_backup_db_name: error 'should specify postgres backup db name',
    postgres_backup_postgres_host: 'pgpool',

    // S3 endpoint and credentials.
    postgres_backup_s3_endpoint: 'https://s3.fr-par.scw.cloud',
    postgres_backup_s3_region: 'fr-par',
    postgres_backup_s3_bucket: error 'should specify postgres backup s3 bucket',
    postgres_backup_s3_access_key_id_secret_name: error 'should specif postgres backup s3 access key id secret name',
    postgres_backup_s3_access_key_id_secret_key: error 'should specify postgres backup s3 access key id secret key',
    postgres_backup_s3_secret_access_key_secret_name: error 'should specify postgres backup s3 secret key secret name',
    postgres_backup_s3_secret_access_key_secret_key: error 'should specify postgres backup s3 secret key secret key',

    // Password to encrypt the backup.
    postgres_backup_password_secret_name: error 'should specify postgres backup password secret name',
    postgres_backup_password_secret_key: error 'should specify postgres backup password secret key',

    // Frequency of the job.
    postgres_backup_crontab: '0 * * * *',
  },

  postgres_backup_dump_container::
    container.new('postgres-backup-dump', $._images.postgres)
    + container.withImagePullPolicy('IfNotPresent')
    + container.withEnvMixin([
      envVar.fromSecretRef('PGPASSWORD', $._config.postgres_backup_postgres_password_secret_name, $._config.postgres_backup_postgres_password_secret_key),
    ])
    + container.withVolumeMounts([
      volumeMount.new('backup-data', '/data'),
    ])
    + container.withCommand([
      'pg_dump',
      '--verbose',
      '--format=c',  // Custom output file format.
      '--file=/data/%s.pgdump' % $._config.postgres_backup_db_name,
      '--compress=9',
      '--host=%s' % $._config.postgres_backup_postgres_host,
      '--username=%s' % $._config.postgres_backup_postgres_username,
      '--dbname=%s' % $._config.postgres_backup_db_name,
    ]),

  postgres_backup_configmap:
    configMap.new('postgres-backup-scripts', {
      'upload-backup.sh': importstr 'postgres-files/upload-backup.sh',
    }),

  postgres_backup_upload_container::
    container.new('postgres-backup-upload', $._images.aws_cli)
    + container.withImagePullPolicy('IfNotPresent')
    + container.withEnvMixin([
      envVar.new('DB_NAME', $._config.postgres_backup_db_name),
      envVar.fromSecretRef('BACKUP_PASSWORD', $._config.postgres_backup_password_secret_name, $._config.postgres_backup_password_secret_key),
      envVar.new('S3_BUCKET', $._config.postgres_backup_s3_bucket),
      envVar.new('S3_ENDPOINT', $._config.postgres_backup_s3_endpoint),
      envVar.new('S3_REGION', $._config.postgres_backup_s3_region),
      envVar.fromSecretRef('S3_ACCESS_KEY_ID', $._config.postgres_backup_s3_access_key_id_secret_name, $._config.postgres_backup_s3_access_key_id_secret_key),
      envVar.fromSecretRef('S3_SECRET_ACCESS_KEY', $._config.postgres_backup_s3_secret_access_key_secret_name, $._config.postgres_backup_s3_secret_access_key_secret_key),
    ])
    + container.withVolumeMounts([
      volumeMount.new('backup-data', '/data'),
      volumeMount.new('postgres-backup-scripts', '/upload-backup.sh') + volumeMount.withSubPath('upload-backup.sh'),
    ])
    + container.withCommand(['bash', '/upload-backup.sh']),

  postgres_backup_job:
    cronJob.new('postgres-backup', $._config.postgres_backup_crontab, $.postgres_backup_upload_container)
    + cronJob.mixin.spec.jobTemplate.spec.template.spec.withInitContainers([$.postgres_backup_dump_container])
    + cronJob.mixin.spec.jobTemplate.spec.template.spec.withRestartPolicy('Never')
    + cronJob.mixin.spec.jobTemplate.spec.template.spec.withVolumes([
      volume.fromEmptyDir('backup-data'),
      volume.fromConfigMap('postgres-backup-scripts', 'postgres-backup-scripts'),
    ])
    + cronJob.mixin.spec.withSuspend(false),
}
