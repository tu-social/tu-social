#!/bin/bash
# FROM: https://github.com/bitnami/charts/blob/1104e32e205a579e92196123f98582898bda3f4f/bitnami/postgresql-ha/templates/postgresql/hooks-scripts-configmap.yaml
# Copyright &copy; 2023 Bitnami
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# <http://www.apache.org/licenses/LICENSE-2.0>
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and limitations under the License.

set -o errexit
set -o pipefail
set -o nounset

# Debug section
exec 3>&1
exec 4>&2

# Process input parameters
MIN_DELAY_AFTER_PG_STOP_SECONDS=$1

# Load Libraries
. /opt/bitnami/scripts/liblog.sh
. /opt/bitnami/scripts/libpostgresql.sh
. /opt/bitnami/scripts/librepmgr.sh

# Load PostgreSQL & repmgr environment variables
. /opt/bitnami/scripts/postgresql-env.sh

# Auxiliary functions
is_new_primary_ready() {
    return_value=1
    currenty_primary_node="$(repmgr_get_primary_node)"
    currenty_primary_host="$(echo $currenty_primary_node | awk '{print $1}')"

    info "$currenty_primary_host != $REPMGR_NODE_NETWORK_NAME"
    if [[ $(echo $currenty_primary_node | wc -w) -eq 2 ]] && [[ "$currenty_primary_host" != "$REPMGR_NODE_NETWORK_NAME" ]]; then
        info "New primary detected, leaving the cluster..."
        return_value=0
    else
        info "Waiting for a new primary to be available..."
    fi
    return $return_value
}

export MODULE="pre-stop-hook"

if [[ "${BITNAMI_DEBUG}" == "true" ]]; then
    info "Bash debug is on"
else
    info "Bash debug is off"
    exec 1>/dev/null
    exec 2>/dev/null
fi

postgresql_enable_nss_wrapper

# Prepare env vars for managing roles
readarray -t primary_node < <(repmgr_get_upstream_node)
primary_host="${primary_node[0]}"

info "Primary node: $primary_node"
info "Primary host: $primary_host"
info "This node network name: $REPMGR_NODE_NETWORK_NAME"

# Stop postgresql for graceful exit.
PG_STOP_TIME=$EPOCHSECONDS
postgresql_stop

if [[ -z "$primary_host" ]] || [[ "$primary_host" == "$REPMGR_NODE_NETWORK_NAME" ]]; then
    info "Primary node need to wait for a new primary node before leaving the cluster"
    retry_while is_new_primary_ready 10 5
else
    info "Standby node doesn't need to wait for a new primary switchover. Leaving the cluster"
fi

# Make sure pre-stop hook waits at least 25 seconds after stop of PG to make sure PGPOOL detects node is down.
# default terminationGracePeriodSeconds=30 seconds
PG_STOP_DURATION=$(($EPOCHSECONDS - $PG_STOP_TIME))
if (( $PG_STOP_DURATION < $MIN_DELAY_AFTER_PG_STOP_SECONDS )); then
    WAIT_TO_PG_POOL_TIME=$(($MIN_DELAY_AFTER_PG_STOP_SECONDS - $PG_STOP_DURATION))
    info "PG stopped including primary switchover in $PG_STOP_DURATION. Waiting additional $WAIT_TO_PG_POOL_TIME seconds for PG pool"
    sleep $WAIT_TO_PG_POOL_TIME
fi