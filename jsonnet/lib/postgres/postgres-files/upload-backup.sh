#!/usr/bin/env bash

set -euo pipefail

# create aws cli credentials
echo "Creating AWS credentials file"
mkdir -p /root/.aws
echo "[default]" >> /root/.aws/credentials
set +x
echo "aws_access_key_id=$S3_ACCESS_KEY_ID" >> /root/.aws/credentials
echo "aws_secret_access_key=$S3_SECRET_ACCESS_KEY" >> /root/.aws/credentials

# create gpg passphrase file
echo "$BACKUP_PASSWORD" > /root/.backup_passphrase

# enable debugging again
set -x

ls /data/

echo "Encrypting backup"
gpg \
  --symmetric \
  --cipher-algo AES256 \
  --batch \
  --passphrase-file=/root/.backup_passphrase \
  "/data/$DB_NAME.pgdump"

echo "Uploading backup"
BACKUP_FILENAME="$DB_NAME.$(date +%s).pgdump.gpg"

aws \
  --endpoint="$S3_ENDPOINT" \
  --region="$S3_REGION" \
  s3 cp "/data/$DB_NAME.pgdump.gpg" "s3://$S3_BUCKET/backups/postgres/all/$BACKUP_FILENAME"

DAILY_BACKUP_FILENAME="$DB_NAME.$(date +'%Y-%m-%d').pgdump.gpg"
echo "Uploading/updating daily backup"

aws \
  --endpoint="$S3_ENDPOINT" \
  --region="$S3_REGION" \
  s3 cp "/data/$DB_NAME.pgdump.gpg" "s3://$S3_BUCKET/backups/postgres/daily/$DAILY_BACKUP_FILENAME"

echo "Shredding the backup"
shred -u /data/*