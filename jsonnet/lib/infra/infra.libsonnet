local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

{
  matchHome():
    k.core.v1.weightedPodAffinityTerm.withWeight(100)
    + k.core.v1.weightedPodAffinityTerm.podAffinityTerm.withTopologyKey('kubernetes.io/hostname')
    + k.core.v1.weightedPodAffinityTerm.podAffinityTerm.labelSelector.withMatchLabelsMixin({ 'topology.kubernetes.io/zone': 'home' }),

  tolerateHome():
    k.core.v1.toleration.withKey('topology.kubernetes.io/zone') +
    k.core.v1.toleration.withOperator('Equal') +
    k.core.v1.toleration.withValue('home') +
    k.core.v1.toleration.withEffect('NoSchedule'),

}
