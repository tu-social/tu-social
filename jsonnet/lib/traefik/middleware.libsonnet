{
  new(name, spec={}): {
    apiVersion: 'traefik.containo.us/v1alpha1',
    kind: 'Middleware',

    metadata: {
      name: name,
    },
    spec: spec,
  },

  basicAuthDefaultName: 'basic-auth',

  newBasicAuth(name=self.basicAuthDefaultName, secretName='basic-auth', headerField=null):
    $.new(name, spec={
      basicAuth: {
        secret: secretName,
        removeHeader: true,
        [if headerField != null then 'headerField']: headerField,
      },
    }),
}
