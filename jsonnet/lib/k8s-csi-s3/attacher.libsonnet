local k = import 'k.libsonnet',
      clusterRole = k.rbac.v1.clusterRole,
      clusterRoleBinding = k.rbac.v1.clusterRoleBinding,
      container = k.core.v1.container,
      envVar = k.core.v1.envVar,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      statefulSet = k.apps.v1.statefulSet,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

// https://github.com/yandex-cloud/k8s-csi-s3/blob/3b38d545abe3cc9b5b29b451bab24fd9a33771d8/deploy/kubernetes/attacher.yaml#L86-L86
{
  csi_attacher_service_account:
    k.core.v1.serviceAccount.new('csi-attacher-sa'),

  external_attacher_runner_cluster_role:
    clusterRole.new('external-attacher-runner') +
    clusterRole.withRulesMixin(
      [
        {
          apiGroups: [''],
          resources: ['secrets'],
          verbs: ['get', 'list'],
        },
        {
          apiGroups: [''],
          resources: ['events'],
          verbs: ['get', 'list', 'watch', 'update'],
        },
        {
          apiGroups: [''],
          resources: ['persistentvolumes'],
          verbs: ['get', 'list', 'watch', 'update'],
        },
        {
          apiGroups: [''],
          resources: ['nodes'],
          verbs: ['get', 'list', 'watch'],
        },
        {
          apiGroups: ['storage.k8s.io'],
          resources: ['csinodes'],
          verbs: ['get', 'list', 'watch'],
        },
        {
          apiGroups: ['storage.k8s.io'],
          resources: ['volumeattachments'],
          verbs: ['get', 'list', 'watch', 'update', 'patch'],
        },
        {
          apiGroups: ['storage.k8s.io'],
          resources: ['volumeattachments/status'],
          verbs: ['patch'],
        },
      ],
    ),

  csi_attacher_cluster_role_binding:
    clusterRoleBinding.new('csi-attacher-role') +
    clusterRoleBinding.mixin.roleRef.withApiGroup('rbac.authorization.k8s.io') +
    clusterRoleBinding.mixin.roleRef.withKind('ClusterRole') +
    clusterRoleBinding.mixin.roleRef.withName('external-attacher-runner') +
    clusterRoleBinding.withSubjectsMixin({
      kind: 'ServiceAccount',
      name: 'csi-attacher-sa',
      namespace: $._config.namespace,
    }),

  csi_attacher_service:
    service.new(
      'csi-attacher-s3',
      { app: 'csi-attacher-s3' },
      servicePort.withName('csi-s3-dummy') + servicePort.withPort(65535),
    ),

  csi_attacher_container::
    container.new('csi-attacher', 'quay.io/k8scsi/csi-attacher:v3.0.1')
    + container.withImagePullPolicy('IfNotPresent')
    + container.withArgs([
      '--v=4',
      '--csi-address=$(ADDRESS)',
    ])
    + container.withEnv([envVar.new('ADDRESS', '/var/lib/kubelet/plugins/ru.yandex.s3.csi/csi.sock')])
    + container.withVolumeMounts([volumeMount.new('socket-dir', '/var/lib/kubelet/plugins/ru.yandex.s3.csi')]),

  csi_attacher_statefulset:
    statefulSet.new('csi-attacher-s3', 1, [$.csi_attacher_container])
    + statefulSet.mixin.spec.withServiceName('csi-attacher-s3')
    + statefulSet.mixin.spec.template.metadata.withLabels({ app: 'csi-attacher-s3' })  // Overwrite labels to remove 'name' to match original YAML.
    + statefulSet.mixin.spec.template.spec.withServiceAccount('csi-attacher-sa')
    + statefulSet.mixin.spec.selector.withMatchLabels({ app: 'csi-attacher-s3' })
    + statefulSet.mixin.spec.template.metadata.withLabelsMixin({ app: 'csi-attacher-s3' })
    + statefulSet.mixin.spec.template.spec.withTolerationsMixin([
      k.core.v1.toleration.withKey('node-role.kubernetes.io/master') + k.core.v1.toleration.withOperator('Exists'),
      k.core.v1.toleration.withKey('CriticalAddonsOnly') + k.core.v1.toleration.withOperator('Exists'),
      k.core.v1.toleration.withOperator('Exists') + k.core.v1.toleration.withEffect('NoSchedule'),
    ])
    + statefulSet.mixin.spec.template.spec.withVolumes([
      volume.fromHostPath('socket-dir', '/var/lib/kubelet/plugins/ru.yandex.s3.csi')
      + volume.hostPath.withType('DirectoryOrCreate'),
    ]),
}
