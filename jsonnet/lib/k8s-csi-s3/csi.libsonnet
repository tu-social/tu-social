// This is a jsonnet version of https://github.com/yandex-cloud/k8s-csi-s3/tree/master/deploy/kubernetes
(import 'attacher.libsonnet')
+ (import 'driver.libsonnet')
+ (import 'provisioner.libsonnet')
+ {
  _config+:: {
    namespace: error 'should provide namespace',
  },
}
