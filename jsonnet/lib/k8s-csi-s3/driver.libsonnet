local k = import 'k.libsonnet',
      clusterRole = k.rbac.v1.clusterRole,
      clusterRoleBinding = k.rbac.v1.clusterRoleBinding,
      container = k.core.v1.container,
      envVar = k.core.v1.envVar,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      daemonSet = k.apps.v1.daemonSet,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

// https://github.com/yandex-cloud/k8s-csi-s3/blob/3b38d545abe3cc9b5b29b451bab24fd9a33771d8/deploy/kubernetes/csi-s3.yaml#L28-L28
{
  csi_s3_service_account:
    k.core.v1.serviceAccount.new('csi-s3'),

  csi_s3_cluster_role:
    clusterRole.new('csi-s3'),

  csi_s3_cluster_role_binding:
    clusterRoleBinding.new('csi-s3') +
    clusterRoleBinding.mixin.roleRef.withApiGroup('rbac.authorization.k8s.io') +
    clusterRoleBinding.mixin.roleRef.withKind('ClusterRole') +
    clusterRoleBinding.mixin.roleRef.withName('csi-s3') +
    clusterRoleBinding.withSubjectsMixin({
      kind: 'ServiceAccount',
      name: 'csi-s3',
      namespace: $._config.namespace,
    }),

  csi_s3_driver_registrar_container::
    container.new('driver-registrar', 'quay.io/k8scsi/csi-node-driver-registrar:v1.2.0')
    + container.withArgs([
      '--kubelet-registration-path=$(DRIVER_REG_SOCK_PATH)',
      '--v=4',
      '--csi-address=$(ADDRESS)',
    ])
    + container.withEnv([
      envVar.new('ADDRESS', '/csi/csi.sock'),
      envVar.new('DRIVER_REG_SOCK_PATH', '/var/lib/kubelet/plugins/ru.yandex.s3.csi/csi.sock'),
      envVar.fromFieldPath('KUBE_NODE_NAME', 'spec.nodeName'),
    ])
    + container.withVolumeMounts([
      volumeMount.new('plugin-dir', '/csi'),
      volumeMount.new('registration-dir', '/registration/'),
    ]),


  csi_s3_container::
    container.new('csi-s3', 'cr.yandex/crp9ftr22d26age3hulg/csi-s3:0.35.4')
    + container.withImagePullPolicy('IfNotPresent')
    + container.withArgs([
      '--endpoint=$(CSI_ENDPOINT)',
      '--nodeid=$(NODE_ID)',
      '--v=4',
    ])
    + container.mixin.securityContext.withPrivileged(true)
    + container.mixin.securityContext.withAllowPrivilegeEscalation(true)
    + container.mixin.securityContext.capabilities.withAdd(['SYS_ADMIN'])
    + container.withEnv([
      envVar.new('CSI_ENDPOINT', 'unix:///csi/csi.sock'),
      envVar.fromFieldPath('NODE_ID', 'spec.nodeName'),
    ])
    + container.withVolumeMounts([
      volumeMount.new('plugin-dir', '/csi'),
      volumeMount.new('stage-dir', '/var/lib/kubelet/plugins/kubernetes.io/csi')
      + volumeMount.withMountPropagation('Bidirectional'),
      volumeMount.new('pods-mount-dir', '/var/lib/kubelet/pods')
      + volumeMount.withMountPropagation('Bidirectional'),
      volumeMount.new('fuse-device', '/dev/fuse'),
      volumeMount.new('systemd-control', '/run/systemd'),
    ]),

  csi_s3_daemonset:
    daemonSet.new('csi-s3', [$.csi_s3_driver_registrar_container, $.csi_s3_container])
    + daemonSet.mixin.spec.template.spec.withHostNetwork(true)
    + daemonSet.mixin.spec.template.spec.withServiceAccount('csi-s3')
    + daemonSet.mixin.spec.template.metadata.withLabelsMixin({ app: 'csi-s3' })
    + daemonSet.mixin.spec.template.metadata.withLabels({ app: 'csi-s3' })  // Overwrite labels to remove 'name' to match original YAML.
    + daemonSet.mixin.spec.selector.withMatchLabels({ app: 'csi-s3' })
    + daemonSet.mixin.spec.template.spec.withTolerationsMixin([
      k.core.v1.toleration.withKey('CriticalAddonsOnly')
      + k.core.v1.toleration.withOperator('Exists'),
      k.core.v1.toleration.withOperator('Exists')
      + k.core.v1.toleration.withEffect('NoExecute')
      + k.core.v1.toleration.withTolerationSeconds(300),
      k.core.v1.toleration.withOperator('Exists')
      + k.core.v1.toleration.withEffect('NoSchedule'),
    ])
    + daemonSet.mixin.spec.template.spec.withVolumes([
      volume.fromHostPath('registration-dir', '/var/lib/kubelet/plugins_registry/')
      + volume.hostPath.withType('DirectoryOrCreate'),
      volume.fromHostPath('plugin-dir', '/var/lib/kubelet/plugins/ru.yandex.s3.csi')
      + volume.hostPath.withType('DirectoryOrCreate'),
      volume.fromHostPath('stage-dir', '/var/lib/kubelet/plugins/kubernetes.io/csi')
      + volume.hostPath.withType('DirectoryOrCreate'),
      volume.fromHostPath('pods-mount-dir', '/var/lib/kubelet/pods')
      + volume.hostPath.withType('Directory'),
      volume.fromHostPath('fuse-device', '/dev/fuse'),
      volume.fromHostPath('systemd-control', '/run/systemd')
      + volume.hostPath.withType('DirectoryOrCreate'),
    ]),
}
