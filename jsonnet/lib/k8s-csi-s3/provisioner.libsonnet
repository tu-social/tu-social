local k = import 'k.libsonnet',
      clusterRole = k.rbac.v1.clusterRole,
      clusterRoleBinding = k.rbac.v1.clusterRoleBinding,
      container = k.core.v1.container,
      envVar = k.core.v1.envVar,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      statefulSet = k.apps.v1.statefulSet,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

// https://github.com/yandex-cloud/k8s-csi-s3/blob/3b38d545abe3cc9b5b29b451bab24fd9a33771d8/deploy/kubernetes/provisioner.yaml#L86-L86
{
  csi_provisioner_service_account:
    k.core.v1.serviceAccount.new('csi-provisioner-sa'),

  external_provisioner_runner_cluster_role:
    clusterRole.new('external-provisioner-runner') +
    clusterRole.withRulesMixin(
      [
        {
          apiGroups: [''],
          resources: ['secrets'],
          verbs: ['get', 'list'],
        },
        {
          apiGroups: [''],
          resources: ['persistentvolumes'],
          verbs: ['get', 'list', 'watch', 'create', 'delete'],
        },
        {
          apiGroups: [''],
          resources: ['persistentvolumeclaims'],
          verbs: ['get', 'list', 'watch', 'update'],
        },
        {
          apiGroups: ['storage.k8s.io'],
          resources: ['storageclasses'],
          verbs: ['get', 'list', 'watch'],
        },
        {
          apiGroups: [''],
          resources: ['events'],
          verbs: ['list', 'watch', 'create', 'update', 'patch'],
        },
      ],
    ),

  csi_provisioner_cluster_role_binding:
    clusterRoleBinding.new('csi-provisioner-role') +
    clusterRoleBinding.mixin.roleRef.withApiGroup('rbac.authorization.k8s.io') +
    clusterRoleBinding.mixin.roleRef.withKind('ClusterRole') +
    clusterRoleBinding.mixin.roleRef.withName('external-provisioner-runner') +
    clusterRoleBinding.withSubjectsMixin({
      kind: 'ServiceAccount',
      name: 'csi-provisioner-sa',
      namespace: $._config.namespace,
    }),

  csi_provisioner_service:
    service.new(
      'csi-provisioner-s3',
      { app: 'csi-provisioner-s3' },
      servicePort.withName('csi-s3-dummy') + servicePort.withPort(65535),
    ),

  csi_provisioner_container::
    container.new('csi-provisioner', 'quay.io/k8scsi/csi-provisioner:v2.1.0')
    + container.withArgs([
      '--csi-address=$(ADDRESS)',
      '--v=4',
    ])
    + container.withEnv([
      //      envVar.new('ADDRESS', '/var/lib/kubelet/plugins/ru.yandex.s3.csi/csi.sock'),
    ])
    + container.withVolumeMounts([
      volumeMount.new('socket-dir', '/var/lib/kubelet/plugins/ru.yandex.s3.csi'),
    ]),

  csi_s3_provisioner_container::
    container.new('csi-s3', 'cr.yandex/crp9ftr22d26age3hulg/csi-s3:0.35.4')
    + container.withImagePullPolicy('IfNotPresent')
    + container.withArgs([
      '--endpoint=$(CSI_ENDPOINT)',
      '--nodeid=$(NODE_ID)',
      '--v=4',
    ])
    + container.withEnv([
      envVar.new('CSI_ENDPOINT', 'unix:///var/lib/kubelet/plugins/ru.yandex.s3.csi/csi.sock'),
      envVar.fromFieldPath('NODE_ID', 'spec.nodeName'),
    ])
    + container.withVolumeMounts([
      volumeMount.new('socket-dir', '/var/lib/kubelet/plugins/ru.yandex.s3.csi'),
    ]),


  csi_provisioner_statefulset:
    statefulSet.new('csi-provisioner-s3', 1, [$.csi_provisioner_container, $.csi_s3_provisioner_container])
    + statefulSet.mixin.spec.withServiceName('csi-provisioner-s3')
    + statefulSet.mixin.spec.template.metadata.withLabels({ app: 'csi-provisioner-s3' })  // Overwrite labels to remove 'name' to match original YAML.
    + statefulSet.mixin.spec.template.spec.withServiceAccount('csi-provisioner-sa')
    + statefulSet.mixin.spec.selector.withMatchLabels({ app: 'csi-provisioner-s3' })
    + statefulSet.mixin.spec.template.metadata.withLabelsMixin({ app: 'csi-provisioner-s3' })
    + statefulSet.mixin.spec.template.spec.withTolerationsMixin([
      k.core.v1.toleration.withKey('node-role.kubernetes.io/master') + k.core.v1.toleration.withOperator('Exists'),
      k.core.v1.toleration.withKey('CriticalAddonsOnly') + k.core.v1.toleration.withOperator('Exists'),
      k.core.v1.toleration.withOperator('Exists')
      + k.core.v1.toleration.withEffect('NoSchedule'),
    ])
    + statefulSet.mixin.spec.template.spec.withVolumes([
      volume.fromEmptyDir('socket-dir', {}),
    ]),
}
