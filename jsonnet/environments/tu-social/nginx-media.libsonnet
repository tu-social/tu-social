local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      configMap = k.core.v1.configMap,
      container = k.core.v1.container,
      pvc = k.core.v1.persistentVolumeClaim,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      statefulSet = k.apps.v1.statefulSet,
      volumeMount = k.core.v1.volumeMount;

local traefikIngress = import 'traefik/ingress.libsonnet';

{
  nginx_configmap:
    configMap.new('nginx-media', {
      'default.conf': importstr 'nginx-media.conf',
    }),

  local container = k.core.v1.container,
  nginx_container::
    container.new('nginx', 'nginx')
    + container.withVolumeMounts([
      volumeMount.new('nginx-media-data', '/data'),
    ])
    + container.withPorts(k.core.v1.containerPort.new('http', 80))
    + k.util.resourcesRequests('50m', '256Mi'),

  nginx_pvc::
    pvc.new()
    + pvc.mixin.metadata.withName('nginx-media-data')
    + pvc.mixin.spec.resources.withRequests({ storage: '10Gi' })
    + pvc.mixin.spec.withAccessModes(['ReadWriteOnce'])
    + pvc.mixin.spec.withStorageClassName('local-path'),

  nginx_statefulset:
    statefulSet.new('nginx-media', 2, [$.nginx_container], [$.nginx_pvc])
    + statefulSet.mixin.spec.withServiceName('nginx-media')
    + statefulSet.mixin.metadata.withLabels({ name: 'nginx-media' })
    + statefulSet.mixin.spec.template.metadata.withLabels({ app: 'nginx-media' })
    + statefulSet.mixin.spec.selector.withMatchLabels({ app: 'nginx-media' })
    + statefulSet.configMapVolumeMount($.nginx_configmap, '/etc/nginx/conf.d'),

  local service = k.core.v1.service,
  local servicePort = k.core.v1.servicePort,
  nginx_service:
    service.new(
      'nginx-media',
      { app: 'nginx-media' },
      servicePort.newNamed('http', 80, 80) + servicePort.withProtocol('TCP')
    )
    + service.mixin.spec.withType('ClusterIP'),

  ingress:
    traefikIngress.new(['media.tu.social'])
    + traefikIngress.withService('nginx-media', port='http', namespace='tu-social'),
}
