local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      secret = k.core.v1.secret;

local postgres = import 'postgres/postgres.libsonnet';
local postgres_backup = import 'postgres/postgres_backup.libsonnet';

postgres + postgres_backup {
  _images+:: {
    postgres: 'docker.io/bitnami/postgresql-repmgr:14-debian-11',
    postgres_exporter: 'quay.io/prometheuscommunity/postgres-exporter:v0.11.1',
    pgpool: 'docker.io/bitnami/pgpool:4',
  },

  _config+:: {
    pgpool_replicas: 2,
    postgres_replicas: 1,

    postgres_secret_name: 'postgres',

    // Backup postgres credentials.
    postgres_backup_postgres_password_secret_name: $._config.postgres_secret_name,
    postgres_backup_postgres_password_secret_key: 'postgresql_password',
    postgres_backup_db_name: 'mastodon_tu_social',

    // Backup S3 config.
    postgres_backup_s3_endpoint: 'https://s3.fr-par.scw.cloud',
    postgres_backup_s3_region: 'fr-par',
    postgres_backup_s3_bucket: 'mastodon-tu-social',
    postgres_backup_s3_access_key_id_secret_name: 'mastodon',
    postgres_backup_s3_access_key_id_secret_key: 's3_access_key_id',
    postgres_backup_s3_secret_access_key_secret_name: 'mastodon',
    postgres_backup_s3_secret_access_key_secret_key: 's3_secret_access_key',

    // Backup password.
    postgres_backup_password_secret_name: $._config.postgres_secret_name,
    postgres_backup_password_secret_key: 'backup_password',
  },

  postgres_secret:
    secret.new($._config.postgres_secret_name, {
      postgresql_password: std.base64(importstr 'secrets/postgres-password.secret.txt'),
      repmgr_password: std.base64(importstr 'secrets/repmgr-password.secret.txt'),
      pgpool_admin_password: std.base64(importstr 'secrets/pgpool-admin-password.secret.txt'),
      backup_password: std.base64(importstr 'secrets/postgres-backup-password.secret.txt'),
    }, 'Opaque'),
}
