# ECK

This folder contains the applied YAML definitions of ECK.
See https://www.elastic.co/guide/en/cloud-on-k8s/master/k8s-deploy-eck.html

## Versions

Please update the versions below if you update the YAML files:

- `https://download.elastic.co/downloads/eck/2.6.1/crds.yaml`
- `https://download.elastic.co/downloads/eck/2.6.1/operator.yaml`