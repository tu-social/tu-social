local secret_test = importstr 'test.secret.txt';
assert secret_test == 'test' : 'Secret not loaded properly, maybe git-crypt is not configured? See https://github.com/AGWA/git-crypt';

{
  _config+:: {
    namespace: 'tu-social',
  },

  redis: (import 'redis.libsonnet') { _config+:: $._config },
  postgres: import 'postgres.libsonnet',
  elasticsearch: import 'elasticsearch.libsonnet',
  mastodon: import 'mastodon.libsonnet',
  nginx_media: import 'nginx-media.libsonnet',

  simple_mail_forwarder: (import 'simple-mail-forwarder.libsonnet') { _config+:: $._config },
  legacy: import 'legacy.libsonnet',

  admin_account: (import 'admin-accounts.libsonnet') { _config+:: $._config },
}
