local traefikIngress = import 'traefik/ingress.libsonnet';
local traefikMiddleware = import 'traefik/middleware.libsonnet';

{
  redirect_to_tu_social_middleware: traefikMiddleware.new('redirect-to-tu.social', {
    // https://doc.traefik.io/traefik/middlewares/http/redirectregex/
    redirectRegex: {
      regex: '^(http|https)://m.tu.social/(.*)',
      replacement: '${1}://tu.social/${2}',
    },
  }),

  // I first set up it on m.tu.social, so this is a temp redirect.
  ingress_m_tu_social:
    traefikIngress.new(['m.tu.social'])
    + traefikIngress.withCustomService({ kind: 'TraefikService', name: 'noop@internal' })
    + traefikIngress.withMiddleware($.redirect_to_tu_social_middleware.metadata.name),

  redirect_to_rick_roll_middleware: traefikMiddleware.new('redirect-to-rick-roll', {
    redirectRegex: {
      regex: '(.*)',
      replacement: 'https://www.youtube.com/watch?v=dQw4w9WgXcQ',
    },
  }),

  ingress_contest_tu_social:
    traefikIngress.new(['contest.tu.social'])
    + traefikIngress.withCustomService({ kind: 'TraefikService', name: 'noop@internal' })
    + traefikIngress.withMiddleware($.redirect_to_rick_roll_middleware.metadata.name),
}
