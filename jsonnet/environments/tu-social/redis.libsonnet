local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local redis = import 'github.com/grafana/jsonnet-libs/redis/redis.libsonnet';

redis {
  _config+:: {
    redis+: {
      password: importstr 'secrets/redis-password.secret.txt',
      diskSize: '5Gi',
      extra_config: 'maxmemory 512mb\n',
    },
  },

  redis_pvc+:: k.core.v1.persistentVolumeClaim.mixin.spec.withStorageClassName('local-path'),
  redis_server_container+:: k.core.v1.container.mixin.resources.withRequestsMixin({
    memory: '512Mi',
    cpu: '100m',
  }),
}
