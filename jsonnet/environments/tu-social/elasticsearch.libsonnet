local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      container = k.core.v1.container,
      containerPort = k.core.v1.containerPort,
      deployment = k.apps.v1.deployment,
      envVar = k.core.v1.envVar;

{
  // Based on https://www.elastic.co/guide/en/cloud-on-k8s/master/k8s-deploy-elasticsearch.html
  // See elasticseach-eck folder for applied CRDs and Operator.
  elasticsearch: {
    apiVersion: 'elasticsearch.k8s.elastic.co/v1',
    kind: 'Elasticsearch',
    metadata: {
      name: 'elastic',
    },
    spec: {
      version: '8.6.2',
      http: {
        tls: {
          selfSignedCertificate: {
            disabled: true,
          },
        },
      },
      nodeSets: [
        {
          name: 'default',
          count: 1,
          config: {
            'node.store.allow_mmap': false,
          },
        },
      ],
    },
  },

  // Elasticserach exporter, manually added seems the operator doesn't seems to have anything related
  _images+:: {
    elastic_exporter: 'quay.io/prometheuscommunity/elasticsearch-exporter:v1.5.0',
  },

  local elasticExporterEnv = [
    envVar.new('ES_USERNAME', 'elastic'),
    envVar.fromSecretRef('ES_PASSWORD', 'elastic-es-elastic-user', 'elastic'),  // Created by Elasticsearch Operator.
  ],

  elastic_exporter_container::
    container.new('elastic-exporter', $._images.elastic_exporter)
    + container.withArgs([
      '--es.uri=http://elastic-es-http:9200',
    ])
    + container.withEnvMixin(elasticExporterEnv)
    + container.withPorts([
      containerPort.new('http-metrics', 9114),
    ])
    + k.util.resourcesRequests('100m', '256Mi'),

  statsd_exporter_deployment: deployment.new(
    name='elastic-exporter',
    replicas=1,
    containers=[$.elastic_exporter_container]
  ),
}
