local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      container = k.core.v1.container,
      containerPort = k.core.v1.containerPort,
      configMap = k.core.v1.configMap,
      cronJob = k.batch.v1.cronJob,
      deployment = k.apps.v1.deployment,
      envVar = k.core.v1.envVar,
      policyRule = k.rbac.v1.policyRule,
      pvc = k.core.v1.persistentVolumeClaim,
      role = k.rbac.v1.role,
      roleBinding = k.rbac.v1.roleBinding,
      secret = k.core.v1.secret,
      service = k.core.v1.service,
      serviceAccount = k.core.v1.serviceAccount,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

local traefikIngress = import 'traefik/ingress.libsonnet';
local traefikMiddleware = import 'traefik/middleware.libsonnet';
local infra = import 'infra/infra.libsonnet';

{
  _images+:: {
    simple_mail_forwarder: 'zixia/simple-mail-forwarder:1.4',
    mastodon_smf_sync: 'registry.gitlab.com/tu-social/mastodon-smf-sync:main-4cd2ea12',
  },

  _config+:: {
    // Namespace where sync job should operate.
    namespace: error 'should set namespace',

    // Secret created by the simple-mail-forwarder-sync job.
    simple_mail_forwarder_secret_name: 'simple-mail-forwarder',

    // simple_mail_forwarder_domain has to be the domain where we send the mail, as it's where SMF will check the DKIM.
    simple_mail_forwarder_domain: 'tu.social',

    simple_mail_forwarder_mastodon_access_token: importstr 'secrets/smf-sync-access-token.secret.txt',

    mastodon_domain: 'tu.social',
    mastodon_url: 'https://tu.social',
  },


  simple_mail_forwarder_syslog_ng_conf_configmap:
    configMap.new('simple-mail-forwarder-syslog-ng-conf', {
      'syslog-ng.conf': importstr 'simple-mail-forwarder-files/syslog-ng.conf',
    }),

  simple_mail_forwarder_container::
    container.new('simple-mail-forwarder', $._images.simple_mail_forwarder)
    + container.withEnvMixin([
      envVar.new('SMF_DOMAIN', $._config.simple_mail_forwarder_domain),
      envVar.fromSecretRef('SMF_CONFIG', $._config.simple_mail_forwarder_secret_name, 'SMF_CONFIG'),
    ])
    + container.withPorts([containerPort.new('smtp', 25)])
    + container.withVolumeMountsMixin([
      volumeMount.new('certificates', '/etc/postfix/cert/smtp.ec.key') + volumeMount.withSubPath('tls.key'),
      volumeMount.new('certificates', '/etc/postfix/cert/smtp.ec.cert') + volumeMount.withSubPath('tls.crt'),
      volumeMount.new('syslog-ng-conf', '/etc/syslog-ng/syslog-ng.conf') + volumeMount.withSubPath('syslog-ng.conf'),
    ])
    + k.util.resourcesRequests('20m', '64Mi'),

  simple_mail_forwarder_dkim_pvc:
    pvc.new('smf-dkim')
    + pvc.mixin.spec.resources.withRequests({ storage: '128Mi' })
    + pvc.mixin.spec.withAccessModes(['ReadWriteOnce'])
    + pvc.mixin.spec.withStorageClassName('local-path'),

  simple_mail_forwarder_deployment:
    deployment.new(
      name='simple-mail-forwarder',
      replicas=2,
      containers=[$.simple_mail_forwarder_container]
    )
    + deployment.pvcVolumeMount('smf-dkim', '/var/db/dkim/')
    + deployment.spec.template.spec.withVolumesMixin(
      [
        volume.fromSecret('certificates', secretName=$.ingress_mail_tu_social.certificate.spec.secretName) + volume.secret.withDefaultMode(256),
        volume.fromConfigMap('syslog-ng-conf', configMapName=$.simple_mail_forwarder_syslog_ng_conf_configmap.metadata.name),
      ]
    ),

  service:
    k.util.serviceFor($.simple_mail_forwarder_deployment)
    + service.spec.withType('LoadBalancer')
    + service.spec.withExternalTrafficPolicy('Local'),  // https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-type-loadbalancer

  simple_mail_forwarder_sync_secret:
    secret.new('smf-sync', {
      mastodon_access_token: std.base64($._config.simple_mail_forwarder_mastodon_access_token),
    }),

  simple_mail_forwarder_sync_container::
    container.new('smf-sync', $._images.mastodon_smf_sync)
    + container.withImagePullPolicy('Always')
    + container.withEnvMixin([
      envVar.fromSecretRef('MASTODON_ACCESS_TOKEN', 'smf-sync', 'mastodon_access_token'),
      envVar.new('MASTODON_DOMAIN', $._config.mastodon_domain),
      envVar.new('MASTODON_URL', $._config.mastodon_url),
      envVar.new('KUBERNETES_NAMESPACE', $._config.namespace),
      envVar.new('KUBERNETES_SECRET_NAME', $._config.simple_mail_forwarder_secret_name),
      envVar.new('KUBERNETES_DEPLOYMENT_NAME', $.simple_mail_forwarder_deployment.metadata.name),
    ]),

  simple_mail_forwarder_sync_job:
    cronJob.new('simple-mail-forwarder-sync', '* * * * *', $.simple_mail_forwarder_sync_container)
    + cronJob.mixin.spec.jobTemplate.spec.template.spec.withRestartPolicy('Never')
    + cronJob.mixin.spec.jobTemplate.spec.template.spec.withServiceAccountName($.simple_mail_forwarder_service_account.metadata.name)
    + cronJob.mixin.spec.withSuspend(false)
    + cronJob.mixin.spec.jobTemplate.spec.withBackoffLimit(2)
    + cronJob.mixin.spec.jobTemplate.spec.template.spec.withTolerationsMixin(infra.tolerateHome())
    + cronJob.mixin.spec.jobTemplate.spec.template.spec.affinity.podAntiAffinity.withPreferredDuringSchedulingIgnoredDuringExecution(infra.matchHome()),

  simple_mail_forwarder_service_account:
    serviceAccount.new('simple-mail-forwarder-sync'),

  simple_mail_forwarder_sync_role:
    role.new('simple-mail-forwarder-sync-role') +
    role.mixin.metadata.withNamespace($._config.namespace) +
    role.withRulesMixin([
      policyRule.withApiGroups('')
      + policyRule.withResources(['secrets'])
      + policyRule.withVerbs(['create']),
      policyRule.withApiGroups('')
      + policyRule.withResources(['secrets'])
      + policyRule.withVerbs(['update', 'get'])
      + policyRule.withResourceNames([$._config.simple_mail_forwarder_secret_name]),
      policyRule.withApiGroups('apps')
      + policyRule.withResources(['deployments'])
      + policyRule.withVerbs(['patch'])
      + policyRule.withResourceNames([$.simple_mail_forwarder_deployment.metadata.name]),
    ]),

  simple_mail_forwarder_sync_rolebinding:
    roleBinding.new('simple-mail-forwarder-sync-rolebinding') +
    roleBinding.mixin.metadata.withNamespace($._config.namespace) +
    roleBinding.mixin.roleRef.withApiGroup('rbac.authorization.k8s.io') +
    roleBinding.mixin.roleRef.withKind('Role') +
    roleBinding.mixin.roleRef.withName($.simple_mail_forwarder_sync_role.metadata.name) +
    roleBinding.withSubjectsMixin({
      kind: 'ServiceAccount',
      name: $.simple_mail_forwarder_service_account.metadata.name,
      namespace: $._config.namespace,
    }),

  ingress_mail_tu_social:
    traefikIngress.new(['mail.tu.social'])
    + traefikIngress.withCustomService({ kind: 'TraefikService', name: 'noop@internal' })
    + traefikIngress.withMiddleware($.redirect_mail_to_tu_social_middleware.metadata.name),

  // TODO: maybe host something here explaining how it works?
  redirect_mail_to_tu_social_middleware: traefikMiddleware.new('redirect-mail-to-tu.social', {
    // https://doc.traefik.io/traefik/middlewares/http/redirectregex/
    redirectRegex: {
      regex: '^(http|https)://mail.tu.social/(.*)',
      replacement: '${1}://tu.social/${2}',
    },
  }),
}
