local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      cronJob = k.batch.v1.cronJob,
      job = k.batch.v1.job,
      configMap = k.core.v1.configMap,
      container = k.core.v1.container,
      deployment = k.apps.v1.deployment,
      envVar = k.core.v1.envVar,
      pvc = k.core.v1.persistentVolumeClaim,
      secret = k.core.v1.secret,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      statefulSet = k.apps.v1.statefulSet,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

local traefikIngress = import 'traefik/ingress.libsonnet';
local infra = import 'infra/infra.libsonnet';

{
  _images+:: {
    mastodon_upgrade: 'docker.io/tootsuite/mastodon:v4.3.1',

    mastodon: 'docker.io/tootsuite/mastodon:v4.3.1',
    mastodon_streaming: 'docker.io/tootsuite/mastodon-streaming:v4.3.1',
  },

  _config+:: {
    mastodon+: {
      replicas: 3,
      sidekiq_replicas: 2,
      streaming_replicas: 2,
    },
  },

  mastodon_secret:
    secret.new('mastodon', {
      secret_key_base: std.base64(importstr 'secrets/mastodon-secret-key-base.secret.txt'),
      otp_secret: std.base64(importstr 'secrets/mastodon-otp-secret.secret.txt'),
      vapid_private_key: std.base64(importstr 'secrets/mastodon-vapid-private-key.secret.txt'),
      vapid_public_key: std.base64(importstr 'secrets/mastodon-vapid-public-key.secret.txt'),
      smtp_login: std.base64(importstr 'secrets/mastodon-smtp-login.secret.txt'),
      smtp_password: std.base64(importstr 'secrets/mastodon-smtp-password.secret.txt'),
      s3_access_key_id: std.base64(importstr 'secrets/s3-access-key-id.secret.txt'),
      s3_secret_access_key: std.base64(importstr 'secrets/s3-secret-access-key.secret.txt'),
      redis_url: std.base64('redis://default:%s@redis-master:6379' % importstr 'secrets/redis-password.secret.txt'),
      active_record_encryption_deterministic_key: std.base64(importstr 'secrets/active-record-encryption-deterministic-key.secret.txt'),
      active_record_encryption_key_derivation_salt: std.base64(importstr 'secrets/active-record-encryption-key-derivation-salt.secret.txt'),
      active_record_encryption_primary_key: std.base64(importstr 'secrets/active-record-encryption-primary-key.secret.txt'),
    }, 'Opaque'),

  mastodon_settings_configmap:
    configMap.new('mastodon-settings', {
      'settings.yml': importstr 'mastodon-files/settings.yml',
    }),

  local mastodonEnv = [
    // Main settings
    envVar.new('LOCAL_DOMAIN', 'tu.social'),
    envVar.new('WEB_DOMAIN', 'tu.social'),

    // Others
    envVar.new('IP_RETENTION_PERIOD', '31556952'),
    envVar.new('SESSION_RETENTION_PERIOD', '31556952'),
    envVar.new('RAILS_ENV', 'production'),
    envVar.new('RAILS_SERVE_STATIC_FILES', 'true'),
    envVar.new('RAILS_LOG_LEVEL', 'info'),
    envVar.new('DEFAULT_LOCALE', 'es'),

    // Redis
    envVar.fromSecretRef('REDIS_URL', 'mastodon', 'redis_url'),

    // Postgres
    envVar.new('DB_HOST', 'pgpool'),
    envVar.new('DB_USER', 'postgres'),
    envVar.new('DB_NAME', 'mastodon_tu_social'),
    envVar.new('DB_PORT', '5432'),
    envVar.fromSecretRef('DB_PASS', 'postgres', 'postgresql_password'),

    // Elastic
    envVar.new('ES_ENABLED', 'true'),
    envVar.new('ES_HOST', 'elastic-es-http'),
    envVar.new('ES_PORT', '9200'),
    envVar.new('ES_USER', 'elastic'),
    envVar.fromSecretRef('ES_PASS', 'elastic-es-elastic-user', 'elastic'),  // Created by Elasticsearch Operator.

    // SMTP
    envVar.new('SMTP_SERVER', 'in-v3.mailjet.com'),
    envVar.new('SMTP_PORT', '587'),
    envVar.new('SMTP_FROM_ADDRESS', 'mastodon' + '@' + 'tu.social'),

    // File storage
    envVar.new('S3_ENABLED', 'true'),
    envVar.new('S3_BUCKET', 'mastodon-tu-social'),
    envVar.new('S3_REGION', 'fr-par'),
    envVar.new('S3_PROTOCOL', 'https'),
    envVar.new('S3_HOSTNAME', 's3.fr-par.scw.cloud'),
    envVar.new('S3_ENDPOINT', 'https://s3.fr-par.scw.cloud'),
    envVar.new('S3_SIGNATURE_VERSION', 's3'),
    envVar.new('S3_OVERRIDE_PATH_STYLE', 'false'),
    envVar.new('S3_ALIAS_HOST', 'media.tu.social'),
    envVar.fromSecretRef('AWS_ACCESS_KEY_ID', 'mastodon', 's3_access_key_id'),
    envVar.fromSecretRef('AWS_SECRET_ACCESS_KEY', 'mastodon', 's3_secret_access_key'),
    envVar.new('S3_MULTIPART_THRESHOLD', '%(bytes)d' % { bytes: 5 * 1024 * 1024 * 1024 }),  // Because of a Scaleway specific issue, see: https://github.com/mastodon/mastodon/issues/22335

    // Streaming
    envVar.new('NODE_ENV', 'production'),
    envVar.new('PORT', '4000'),
    envVar.new('STREAMING_CLUSTER_NUM', '1'),

    // Secrets
    envVar.fromSecretRef('SECRET_KEY_BASE', 'mastodon', 'secret_key_base'),
    envVar.fromSecretRef('OTP_SECRET', 'mastodon', 'otp_secret'),
    envVar.fromSecretRef('VAPID_PRIVATE_KEY', 'mastodon', 'vapid_private_key'),
    envVar.fromSecretRef('VAPID_PUBLIC_KEY', 'mastodon', 'vapid_public_key'),
    envVar.fromSecretRef('SMTP_LOGIN', 'mastodon', 'smtp_login'),
    envVar.fromSecretRef('SMTP_PASSWORD', 'mastodon', 'smtp_password'),

    // Active Record Encryption
    envVar.fromSecretRef('ACTIVE_RECORD_ENCRYPTION_DETERMINISTIC_KEY', 'mastodon', 'active_record_encryption_deterministic_key'),
    envVar.fromSecretRef('ACTIVE_RECORD_ENCRYPTION_KEY_DERIVATION_SALT', 'mastodon', 'active_record_encryption_key_derivation_salt'),
    envVar.fromSecretRef('ACTIVE_RECORD_ENCRYPTION_PRIMARY_KEY', 'mastodon', 'active_record_encryption_primary_key'),

    // OTEL endpoint
    envVar.fromFieldPath('HOST_IP', 'status.hostIP'),
    envVar.new('OTEL_EXPORTER_OTLP_ENDPOINT', 'http://$(HOST_IP):4318'),
  ],

  local mastodonSidekiqEnv = mastodonEnv + [
    envVar.new('DB_POOL', '10'),
  ],

  mastodon_migration_container::
    container.new('migration', $._images.mastodon)
    + container.withCommand(['bash', '-c', 'rm -f /mastodon/tmp/pids/server.pid; bundle exec rails db:migrate'])
    + container.withEnv(mastodonEnv)
    + container.mixin.resources.withRequestsMixin({ cpu: '50m', memory: '256Mi' })
    + container.mixin.resources.withLimitsMixin({ cpu: '2', memory: '2Gi' }),

  mastodon_container::
    container.new('mastodon', $._images.mastodon)
    + container.withCommand(['bash', '-c', 'rm -f /mastodon/tmp/pids/server.pid; bundle exec rails s -p 3000'])
    + container.withPorts([k.core.v1.containerPort.new('web', 3000)])
    + container.withEnv(mastodonEnv)
    + container.mixin.resources.withRequestsMixin({ cpu: '200m', memory: '1Gi' })
    + container.mixin.resources.withLimitsMixin({ cpu: '2', memory: '2Gi' }),

  mastodon_deployment:
    deployment.new('mastodon', $._config.mastodon.replicas, $.mastodon_container)
    + deployment.mixin.spec.template.spec.withInitContainersMixin($.mastodon_migration_container)
    + deployment.mixin.spec.template.metadata.withLabels({ app: 'mastodon' })
    + deployment.mixin.spec.selector.withMatchLabels({ app: 'mastodon' })
    + deployment.configMapVolumeMount($.mastodon_settings_configmap, '/opt/mastodon/settings.yml', volumeMount.withSubPath('settings.yml')),

  mastodon_service: service.new(
    'mastodon',
    { app: 'mastodon' },
    [servicePort.newNamed('web', 80, 'web') + servicePort.withProtocol('TCP')],
  ),

  mastodon_sidekiq_container::
    container.new('mastodon-sidekiq', $._images.mastodon)
    + container.withCommand(['bash', '-c', 'bundle exec sidekiq -c 10'])
    + container.withEnv(mastodonSidekiqEnv)
    + container.mixin.resources.withRequestsMixin({ cpu: '250m', memory: '512Mi' })
    + container.mixin.resources.withLimitsMixin({ cpu: '2', memory: '2Gi' }),

  mastodon_sidekiq_deployment:
    deployment.new('mastodon-sidekiq', $._config.mastodon.sidekiq_replicas, $.mastodon_sidekiq_container)
    + deployment.mixin.spec.selector.withMatchLabels({ app: 'mastodon-sidekiq' })
    + deployment.mixin.spec.template.metadata.withLabels({ app: 'mastodon-sidekiq' })
    + deployment.mixin.spec.template.spec.withInitContainersMixin($.mastodon_migration_container)
    + deployment.configMapVolumeMount($.mastodon_settings_configmap, '/opt/mastodon/settings.yml', volumeMount.withSubPath('settings.yml'))
    + deployment.mixin.spec.template.spec.withTolerationsMixin(infra.tolerateHome())
    + deployment.spec.template.spec.affinity.podAntiAffinity.withPreferredDuringSchedulingIgnoredDuringExecution(infra.matchHome()),

  mastodon_streaming_container::
    container.new('mastodon-streaming', $._images.mastodon_streaming)
    + container.withPorts([k.core.v1.containerPort.new('streaming', 4000)])
    + container.withCommand(['bash', '-c', '/usr/local/bin/node ./streaming'])
    + container.withEnv(mastodonEnv)
    + container.mixin.resources.withRequestsMixin({ cpu: '50m', memory: '256Mi' })
    + container.mixin.resources.withLimitsMixin({ cpu: '2', memory: '1Gi' }),

  mastodon_streaming_deployment:
    deployment.new('mastodon-streaming', $._config.mastodon.streaming_replicas, $.mastodon_streaming_container)
    + deployment.mixin.spec.template.spec.withInitContainersMixin($.mastodon_migration_container)
    + deployment.mixin.spec.template.metadata.withLabels({ app: 'mastodon-streaming' })
    + deployment.mixin.spec.selector.withMatchLabels({ app: 'mastodon-streaming' })
    + deployment.configMapVolumeMount($.mastodon_settings_configmap, '/opt/mastodon/settings.yml', volumeMount.withSubPath('settings.yml')),

  mastodon_streaming_service: service.new(
    'mastodon-streaming',
    { app: 'mastodon-streaming' },
    [servicePort.newNamed('streaming', 80, 'streaming') + servicePort.withProtocol('TCP')],
  ),

  ingress:
    traefikIngress.new(['tu.social'])
    + traefikIngress.withService('mastodon', port='web', namespace='tu-social')
    + traefikIngress.withRoutePrefixService('mastodon-streaming', prefix='/api/v1/streaming', port='streaming', namespace='tu-social'),

  mastodon_maintenance_container::
    container.new('mastodon-maintenance', $._images.mastodon)
    + container.withImagePullPolicy('IfNotPresent')
    + container.withEnv(mastodonEnv)
    + container.mixin.resources.withLimitsMixin({ cpu: '1', memory: '2Gi' })
    + container.withVolumeMounts([
      volumeMount.new('mastodon-maintenance-scripts', '/mastodon-maintenance.sh') + volumeMount.withSubPath('mastodon-maintenance.sh'),
    ])
    + container.withCommand(['bash', '/mastodon-maintenance.sh']),

  mastodon_maintenance_configmap:
    configMap.new('mastodon-maintenance-scripts', {
      'mastodon-maintenance.sh': importstr 'mastodon-files/mastodon-maintenance.sh',
    }),

  postgres_backup_job:
    cronJob.new('mastodon-maintenance', '@daily', $.mastodon_maintenance_container)
    + cronJob.mixin.spec.jobTemplate.spec.template.spec.withRestartPolicy('Never')
    + cronJob.mixin.spec.jobTemplate.spec.template.spec.withVolumes([
      volume.fromConfigMap('mastodon-maintenance-scripts', 'mastodon-maintenance-scripts'),
    ])
    + cronJob.mixin.spec.withSuspend(false),

  mastodon_pre_upgrade_migration_container::
    $.mastodon_migration_container
    + container.mixin.withImage($._images.mastodon_upgrade)
    + container.withEnvMixin([envVar.new('SKIP_POST_DEPLOYMENT_MIGRATIONS', 'true')]),

  mastodon_pre_upgrade_migration_job: if $._images.mastodon == $._images.mastodon_upgrade then null else
    job.new('pre-upgrade-migration')
    + job.spec.template.spec.withRestartPolicy('Never')
    + job.spec.template.spec.withContainers($.mastodon_pre_upgrade_migration_container)
    + job.configMapVolumeMount($.mastodon_settings_configmap, '/opt/mastodon/settings.yml', volumeMount.withSubPath('settings.yml')),
}
