#!/usr/bin/env bash

set -euo pipefail

echo "Removing media older than 31 days"
tootctl media remove --days 31

echo "Remove link previews older than 365 days"
tootctl preview_cards remove --days 365

echo "Remove files not linked to any post"
tootctl media remove-orphans

echo "Remove remote accounts that never interacted with local users"
tootctl accounts prune

echo "Remove unreferenced statuses from the database"
tootctl statuses remove --days 31

echo "Recreate elasticsearch indexes"
tootctl search deploy