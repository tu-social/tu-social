local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local oauth2_proxy = import 'github.com/grafana/jsonnet-libs/oauth2-proxy/oauth2-proxy.libsonnet';
local ingress = import 'traefik/ingress.libsonnet';
local middleware = import 'traefik/middleware.libsonnet';

oauth2_proxy {
  _images+:: {
    oauth2_proxy: 'quay.io/oauth2-proxy/oauth2-proxy:v7.4.0',
  },

  _config:: {
    oauth_cookie_secret: importstr 'secrets/oauth2-cookie-secret.secret.txt',
    oauth_client_id: importstr 'secrets/oauth2-client-id.secret.txt',
    oauth_client_secret: importstr 'secrets/oauth2-client-secret.secret.txt',
    oauth_redirect_url: 'https://oauth2.tu.social/oauth2/callback',
    oauth_upstream: 'static://202',  // see https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview/#configuring-for-use-with-the-traefik-v2-forwardauth-middleware
    oauth_pass_basic_auth: 'false',
    oauth_extra_args: [
      // This will make oauth2-proxy read the X-Forwarded-For-* headers that Traefik sends, to properly identify the redirect.
      '--reverse-proxy=true',
      // If we don't set a proxy-prefix, then oauth2-proxy will strip it out because the redirectURI contains the empty proxy-prefix
      '--proxy-prefix=/oauth2/',
      // Set the cookie for all .tu.social subdomains.
      '--cookie-domain=.tu.social',
      // Allow redirect to be any of the *.tu.social subdomains.
      '--whitelist-domain=*.tu.social',
      // Users will hit foobar.tu.social and we'll show them the oauth2-proxy page,
      // if we don't skip the provider buttin, it will route them to foobar.tu.social/oauth2/start, which doesn't exist.
      // A workaround is to add `/oauth2/` prefix interception middleware on all authenticated pages, but this is just easier.
      '--skip-provider-button=true',
      '--authenticated-emails-file=/etc/oauth2/authenticated_emails/emails',
    ],

    oauth_email_domain: 'nobody.tu.social',
    oauth_authenticated_emails: [
      'lambroso@gmail.com',
      'dpaneda@gmail.com',
      'netsuso@gmail.com',
    ],
  },

  oauth2_proxy_authenticated_emails_configmap: k.core.v1.configMap.new('oauth2-proxy-authenticated-emails', {
    emails: std.join('\n', $._config.oauth_authenticated_emails),
  }),
  oauth2_proxy_deployment+: k.apps.v1.deployment.configMapVolumeMount($.oauth2_proxy_authenticated_emails_configmap, '/etc/oauth2/authenticated_emails'),

  forward_auth_middlware: middleware.new('oauth2-proxy', {
    forwardAuth: {
      // We have to reach oauth2-proxy on the internal URL, as the external one will drop the X-Forwarded-For-* headers.
      address: 'http://oauth2-proxy:4180/oauth/',
      trustForwardHeader: true,
      // What the docs say: https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/overview/#configuring-for-use-with-the-traefik-v2-forwardauth-middleware
      authResponseHeaders: [
        'X-Auth-Request-Access-Token',
        'Authorization',
      ],
    },
  }),

  auth_headers_middlware: middleware.new('auth-headers', {
    headers: {
      stsSeconds: 315360000,
      browserXssFilter: true,
      contentTypeNosniff: true,
      forceSTSHeader: true,
      stsIncludeSubdomains: true,
      stsPreload: true,
      frameDeny: true,
      // TODO: apparently these two are deprecated. Logs said:
      // SSLRedirect is deprecated, please use entrypoint redirection instead.
      // SSLHost is deprecated, please use RedirectRegex middleware instead.
      sslRedirect: true,
      sslHost: 'tu.social',
    },
  }),

  ingress:
    ingress.new(['oauth2.tu.social'])
    + ingress.withMiddleware('auth-headers')
    + ingress.withService('oauth2-proxy', port='oauth2-proxy-http', namespace='traefik'),
}
