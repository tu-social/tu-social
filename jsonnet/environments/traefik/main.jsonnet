local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local oauth2_proxy = import 'oauth2_proxy.libsonnet';
local ingress = import 'traefik/ingress.libsonnet';
local middleware = import 'traefik/middleware.libsonnet';

local secret = k.core.v1.secret;

local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);

{
  _config+:: {
    namespace: 'traefik',
  },

  oauth2_proxy: oauth2_proxy,

  namespace: k.core.v1.namespace.new($._config.namespace),

  ingress: ingress.new(['traefik.tu.social'])
           + ingress.withMiddleware('oauth2-proxy')
           + ingress.withCustomService({ kind: 'TraefikService', name: 'api@internal' }),

  traefik: helm.template('traefik', './charts/traefik', {
    namespace: 'traefik',
    values: {
      dashboard: { enabled: true },
      rbac: { enabled: true },
      nodeSelector: { 'node-role.kubernetes.io/master': 'true' },
      additionalArguments: [
        '--api.dashboard=true',
        '--log.level=INFO',
        '--providers.kubernetescrd.allowCrossNamespace=true',
      ],
      // Prometheus metrics port should end in `-metrics` in order to be scraped.
      // So we drop the `metrics` port and add an `http-metrics` one.
      // https://github.com/grafana/jsonnet-libs/blob/5fb2525/prometheus/scrape_configs.libsonnet#L25-L30
      ports: {
        metrics: null,
        'http-metrics': {
          port: 9100,
          protocol: 'TCP',
          expose: false,
        },
      },
      metrics: {
        prometheus: {
          entryPoint: 'http-metrics',
        },
      },
      // Pod should have a `name` label in order to be scraped.
      // https://github.com/grafana/jsonnet-libs/blob/5fb2525/prometheus/scrape_configs.libsonnet#L32-L37
      deployment: {
        annotations: {
          'tu.social/deployed-by': 'jsonnet',  // Since k3s is trying to deploy its own Traefik, we want to have this one identified at all times.
        },
        podLabels: {
          name: 'traefik',
        },
      },

      service: {
        annotations: {
          'tu.social/deployed-by': 'jsonnet',  // Since k3s is trying to deploy its own Traefik, we want to have this one identified at all times.
        },
        spec: {
          // https://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/#preserving-the-client-source-ip
          externalTrafficPolicy: 'Local',
        },
      },
    },
  }),
}
