local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      configMap = k.core.v1.configMap,
      container = k.core.v1.container,
      containerPort = k.core.v1.containerPort,
      deployment = k.apps.v1.deployment,
      envVar = k.core.v1.envVar,
      persistentVolumeClaim = k.core.v1.persistentVolumeClaim,
      secret = k.core.v1.secret,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

local traefikIngress = import 'traefik/ingress.libsonnet';
local postgres = import 'postgres/postgres.libsonnet';
local postgres_backup = import 'postgres/postgres_backup.libsonnet';

{
  namespace: k.core.v1.namespace.new($._config.namespace),

  _images+:: {
    vaultwarden: 'vaultwarden/server:1.32.3',
  },

  _config+:: {
    namespace: 'vaultwarden',
  },

  vaultwarden: {
    smtp_secret:
      secret.new('smtp', {
        // We import same mastodon credentials because free Mailjet only allows us 1 subapp.
        smtp_username: std.base64(importstr 'secrets/mastodon-smtp-login.secret.txt'),
        smtp_password: std.base64(importstr 'secrets/mastodon-smtp-password.secret.txt'),
      }, 'Opaque'),

    data_pvc:
      persistentVolumeClaim.new('vaultwarden-data') +
      persistentVolumeClaim.mixin.spec.withAccessModes(['ReadWriteMany']) +
      persistentVolumeClaim.mixin.spec.withStorageClassName('scaleway-s3-retain') +
      persistentVolumeClaim.mixin.spec.resources.withRequests({ storage: '10Gi' }),

    attachments_pvc:
      persistentVolumeClaim.new('vaultwarden-attachments') +
      persistentVolumeClaim.mixin.spec.withAccessModes(['ReadWriteMany']) +
      persistentVolumeClaim.mixin.spec.withStorageClassName('scaleway-s3-retain') +
      persistentVolumeClaim.mixin.spec.resources.withRequests({ storage: '10Gi' }),

    secret:
      secret.new('vaultwarden', {
        admin_token: std.base64(importstr 'secrets/admin-token.secret.txt'),
        // Got these from https://bitwarden.com/host/ with oleg@tu.social email.
        bitwarden_installation_id: std.base64(importstr 'secrets/bitwarden-installation-id.secret.txt'),
        bitwarden_installation_key: std.base64(importstr 'secrets/bitwarden-installation-key.secret.txt'),
      }, 'Opaque'),

    container::
      container.new('vaultwarden', $._images.vaultwarden)
      + container.withPorts([containerPort.new('http', 80)])
      + container.withEnv([
        // Config
        envVar.new('DOMAIN', 'https://vault.tu.social'),
        envVar.new('SIGNUPS_DOMAINS_WHITELIST', 'tu.social'),  // Use tu.social's email forwarding service to register.
        envVar.new('SIGNUPS_VERIFY', 'true'),  // Don't allow registering with someone else's email.
        envVar.new('SHOW_PASSWORD_HINT', 'false'),  // We have SMTP, so send them by email.
        envVar.new('DATA_FOLDER', '/data'),
        envVar.new('ATTACHMENTS_FOLDER', '/attachments'),
        envVar.new('ICON_CACHE_FOLDER', '/icons-cache'),

        // Push notifications: https://github.com/dani-garcia/vaultwarden/pull/3304
        envVar.new('PUSH_ENABLED', 'true'),
        envVar.fromSecretRef('PUSH_INSTALLATION_ID', 'vaultwarden', 'bitwarden_installation_id'),
        envVar.fromSecretRef('PUSH_INSTALLATION_KEY', 'vaultwarden', 'bitwarden_installation_key'),

        // Admin auth
        envVar.fromSecretRef('ADMIN_TOKEN', 'vaultwarden', 'admin_token'),

        // Postgres
        envVar.new('DATABASE_URL', 'postgresql://pgpool:5432/vaultwarden_tu_social'),
        envVar.new('PGUSER', 'postgres'),
        envVar.fromSecretRef('PGPASSWORD', 'postgres', 'postgresql_password'),

        // SMTP
        envVar.new('SMTP_HOST', 'in-v3.mailjet.com'),
        envVar.new('SMTP_PORT', '587'),
        envVar.new('SMTP_FROM', 'vault' + '@' + 'tu.social'),
        envVar.new('SMTP_SECURITY', 'starttls'),
        envVar.fromSecretRef('SMTP_USERNAME', 'smtp', 'smtp_username'),
        envVar.fromSecretRef('SMTP_PASSWORD', 'smtp', 'smtp_password'),
      ])
      + container.mixin.resources.withRequestsMixin({ cpu: '250m', memory: '256Mi' })
      + container.mixin.resources.withLimitsMixin({ cpu: '2', memory: '1Gi' })
      + container.withVolumeMounts([
        volumeMount.new('data', '/data'),
        volumeMount.new('attachments', '/attachments'),
        volumeMount.new('icons-cache', '/icons-cache'),
      ]),

    deployment:
      deployment.new('vaultwarden', 2, self.container)
      + deployment.mixin.spec.template.metadata.withLabels({ app: 'vaultwarden' })
      + deployment.mixin.spec.selector.withMatchLabels({ app: 'vaultwarden' })
      + deployment.mixin.spec.template.spec.withVolumes([
        volume.fromPersistentVolumeClaim('data', 'vaultwarden-data'),
        volume.fromPersistentVolumeClaim('attachments', 'vaultwarden-attachments'),
        volume.fromEmptyDir('icons-cache', {}),
      ]),

    service: k.util.serviceFor(self.deployment),

    ingress:
      traefikIngress.new(['vault.tu.social'])
      + traefikIngress.withService('vaultwarden', 'vaultwarden-http')
      + traefikIngress.withRoutePrefixService(
        'vaultwarden', '/admin', port='vaultwarden-http', namespace='vaultwarden', middlewares=[
          { namespace: 'traefik', name: 'oauth2-proxy' },
        ]
      ),
  },


  postgres+: postgres + postgres_backup {
    _images+:: $._images,

    _config+:: {
      pgpool_replicas: 2,
      postgres_replicas: 1,

      postgres_secret_name: 'postgres',

      // Backup postgres credentials.
      postgres_backup_postgres_password_secret_name: 'postgres',
      postgres_backup_postgres_password_secret_key: 'postgresql_password',
      postgres_backup_db_name: 'vaultwarden_tu_social',

      // Backup S3 config.
      postgres_backup_s3_endpoint: 'https://s3.fr-par.scw.cloud',
      postgres_backup_s3_region: 'fr-par',
      postgres_backup_s3_bucket: 'vaultwarden-tu-social',

      postgres_backup_s3_access_key_id_secret_name: 'postgres-backup',
      postgres_backup_s3_access_key_id_secret_key: 's3_access_key_id',
      postgres_backup_s3_secret_access_key_secret_name: 'postgres-backup',
      postgres_backup_s3_secret_access_key_secret_key: 's3_secret_access_key',

      // Backup password.
      postgres_backup_password_secret_name: 'postgres-backup',
      postgres_backup_password_secret_key: 'backup_password',
    },

    postgres_secret:
      secret.new('postgres', {
        postgresql_password: std.base64(importstr 'secrets/postgres-password.secret.txt'),
        repmgr_password: std.base64(importstr 'secrets/repmgr-password.secret.txt'),
        pgpool_admin_password: std.base64(importstr 'secrets/pgpool-admin-password.secret.txt'),
      }, 'Opaque'),

    postgres_backup_secret:
      secret.new('postgres-backup', {
        backup_password: std.base64(importstr 'secrets/postgres-backup-password.secret.txt'),
        s3_access_key_id: std.base64(importstr 'secrets/s3-access-key-id.secret.txt'),
        s3_secret_access_key: std.base64(importstr 'secrets/s3-secret-access-key.secret.txt'),
      }, 'Opaque'),
  },
}
