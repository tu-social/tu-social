local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      daemonSet = k.apps.v1.daemonSet,
      configMap = k.core.v1.configMap,
      pvc = k.core.v1.persistentVolumeClaim,
      statefulSet = k.apps.v1.statefulSet,
      container = k.core.v1.container,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      containerPort = k.core.v1.containerPort,
      secret = k.core.v1.secret;

local mimir_mixin = import 'github.com/grafana/mimir/operations/mimir-mixin/mixin.libsonnet';
local traefik_mixin = import 'github.com/grafana/jsonnet-libs/traefik-mixin/mixin.libsonnet';
local postgres_mixin = import 'github.com/prometheus-community/postgres_exporter/postgres_mixin/mixin.libsonnet';
local redis_mixin = import 'github.com/grafana/jsonnet-libs/redis-mixin/mixin.libsonnet';

local prometheus = import 'prometheus-ksonnet/prometheus-ksonnet.libsonnet';
local ingress = import 'traefik/ingress.libsonnet';
local middleware = import 'traefik/middleware.libsonnet';
local grafana_cloud_o11y = import 'grafana_cloud_o11y.libsonnet';
{
  _config+:: {
    cluster_name: 'tu.social',
    namespace: 'default',
  },
  _images+:: {
    promtail: 'grafana/promtail:2.4.2',
    grafana_agent: 'grafana/agent:v0.23.0',
  },

  namespace: k.core.v1.namespace.new($._config.namespace),

  // This is not just a prometheus, it's also a grafana, rules, dashboards, etc.
  prometheus: prometheus {
    local prometheusKsonnet = self,
    _images+: {
      grafana: 'grafana/grafana:11.3.0',
      prometheus: 'prom/prometheus:v2.44.0',
    },
    _config+:: $._config {
      grafana_root_url: 'https://grafana.tu.social',

      grafana_ini+: {
        sections+: {
          feature_toggles: {
            enable: std.join(' ', [
              'autoMigrateOldPanels',
            ]),
          },
        },
      },

      // We don't need this alertmanager, we have one in Mimir.
      alertmanager_cluster_self+: {
        replicas: 0,
        global: false,
      },
    },
    // Increase the default 200m to avoid cpu throttling alert.
    // It usually consumes 0 cpu.
    node_exporter_container+:: k.util.resourcesLimits('1', '100Mi'),

    local mimirWritesDefaultPassword = 'mimir-write-default-password',

    default_mimir_writes_password_secret:
      secret.new(mimirWritesDefaultPassword, { [mimirWritesDefaultPassword]: std.base64(importstr 'secrets/mimir-write-default.secret.password.txt') }, 'Opaque'),

    prometheus+:
      {
        _config+: {
          prometheus_requests_cpu: '250m',
          prometheus_requests_memory: '1Gi',
          prometheus_limits_cpu: null,
          prometheus_limits_memory: '2Gi',
        },
        prometheus_pvc+:: pvc.mixin.spec.resources.withRequests({ storage: '32Gi' }),

        prometheus_container+:: container.withVolumeMountsMixin([volumeMount.new(mimirWritesDefaultPassword, '/mimir-auth')]),
        prometheus_statefulset+: statefulSet.mixin.spec.template.spec.withVolumesMixin([volume.fromSecret(mimirWritesDefaultPassword, mimirWritesDefaultPassword)]),
      },

    prometheus_config+: {
      global+: {
        scrape_interval: '30s',
      },
      remote_write: [
        {
          basic_auth: { username: 'default', password_file: '/mimir-auth/' + mimirWritesDefaultPassword },
          url: 'https://mimir-write.tu.social/api/v1/push',
        },
      ],
      scrape_configs: [
        config {
          relabel_configs+:
            [
              {
                // Add 'cluster' label to all metrics, this is required by some Grafana-authored mixins like mimir-mixin.
                target_label: 'cluster',
                replacement: $._config.cluster_name,
              },
            ],
        }
        for config in super.scrape_configs
      ],
    },

    local mimirReadsDefaultPassword = 'mimir-read-default-password',

    default_mimir_reads_password_secret:
      secret.new(mimirReadsDefaultPassword, { [mimirReadsDefaultPassword]: std.base64(importstr 'secrets/mimir-read-default.secret.password.txt') }, 'Opaque'),

    local tusocialGrafanaNetEditorAPIKey = 'tusocial-grafana-net-editor-api-key',
    tusocial_grafana_net_editor_api_key_secret:
      secret.new(tusocialGrafanaNetEditorAPIKey, { [tusocialGrafanaNetEditorAPIKey]: std.base64(importstr 'secrets/grafana-cloud-tusocial.secret.editor-api-key.txt') }, 'Opaque'),

    grafana_container+:: container.withEnvMixin([
      k.core.v1.envVar.fromSecretRef(
        'MIMIR_READS_DEFAULT_PASSWORD',
        mimirReadsDefaultPassword,
        mimirReadsDefaultPassword,
      ),
      k.core.v1.envVar.fromSecretRef(
        'TUSOCIAL_GRAFANA_NET_EDITOR_API_KEY',
        tusocialGrafanaNetEditorAPIKey,
        tusocialGrafanaNetEditorAPIKey,
      ),
    ]),

    grafanaDatasources+:: {
      // Override the default Prometheus datasource making it non-default, and also rename it to prometheus-internal.
      'prometheus.yml': prometheusKsonnet.grafana_datasource(
        'prometheus-internal',
        'http://prometheus.%(prometheus_namespace)s.svc.%(cluster_dns_suffix)s%(prometheus_web_route_prefix)s' % prometheusKsonnet._config,
        default=false
      ),

      'mimir.yml': $.prometheus.grafana_datasource_with_basicauth(
        'default@mimir',  // It's the 'default' tenant at 'mimir'
        'https://mimir-read.tu.social/prometheus',
        username='default',
        password='$MIMIR_READS_DEFAULT_PASSWORD',
        default=true,
        method='POST'
      ) + $.prometheus.datasource.withJsonData({
        prometheusType: 'Mimir',
        prometheusVersion: '2.3.0',
      }),

      'loki-tusocial.grafana.net.yml': $.prometheus.grafana_datasource_with_basicauth(
        'loki-tusocial.grafana.net',
        'https://logs-prod-eu-west-0.grafana.net',
        username='585831',
        password='$TUSOCIAL_GRAFANA_NET_EDITOR_API_KEY',
        default=false,
        method='POST',
        type='loki'
      ),
    },

    mixins+:: {
      mimir: mimir_mixin,
      traefik: traefik_mixin,
      postgres: postgres_mixin,
      redis: redis_mixin,

      node_exporter_full: {},  // See: https://github.com/grafana/jsonnet-libs/issues/938

      // See also: https://ipng.ch/s/articles/2022/11/27/mastodon-3.html
      mastodon: {
        grafanaDashboardFolder: 'Mastodon',
        grafanaDashboards+:: {
          // Source: https://grafana.com/grafana/dashboards/17492-mastodon-stats/
          // However, that dashboard can't be provisioned, as it's not using template vars.
          // So I:
          // - took the JSON from there
          // - went to Grafana->Import from json
          // - selected default@mimir datasource
          // - Sent to settings -> JSON Model and copied that json.
          'mastodon-stats.json': (import 'dashboards/mastodon-stats.json'),
        },
      },
      elasticsearch: {
        grafanaDashboardFolder: 'ElasticSearch',
        grafanaDashboards+:: {
          // Source: https://grafana.com/grafana/dashboards/14191-elasticsearch-overview/
          'elasticsearch-overview.json': (import 'dashboards/elasticsearch-overview.json'),
        },
      },
    },
  },

  ingress: {
    grafana: ingress.new(['grafana.tu.social'])
             + ingress.withMiddleware('oauth2-proxy', 'traefik')
             + ingress.withService('grafana'),
    prometheus: ingress.new(['prometheus.tu.social'])
                + ingress.withMiddleware('oauth2-proxy', 'traefik')
                + ingress.withService('prometheus', 9090),
  },

  grafana_cloud_o11y: grafana_cloud_o11y {
    _images+:: $._images,
    _config+:: $._config,
  },

  grafana_alloy_configmap:
    configMap.new('grafana-alloy', {
      'config.alloy': importstr 'grafana.alloy',
    }),

  grafana_alloy_container::
    container.new('alloy', 'grafana/alloy:latest')
    + container.withPorts(
      containerPort.newNamed(4318, 'otel-http')
      + containerPort.withHostPort(4318)
    )
    + container.withEnvMixin([
      k.core.v1.envVar.fromSecretRef('GRAFANA_CLOUD_TOKEN', 'grafana-cloud-tusocial-writes-api-key', 'api_key.txt'),
    ])
    + k.util.resourcesRequests('0.5', '128Mi'),

  grafana_alloy_daemonset:
    daemonSet.new('grafana-alloy', [$.grafana_alloy_container])
    + statefulSet.configMapVolumeMount($.grafana_alloy_configmap, '/etc/alloy', {}, volume.configMap.withDefaultMode(std.parseOctal('0755'))),
}
