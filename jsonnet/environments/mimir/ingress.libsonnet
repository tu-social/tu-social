local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      secret = k.core.v1.secret;

local ingress = import 'traefik/ingress.libsonnet';
local middleware = import 'traefik/middleware.libsonnet';

{
  ingress: {
    reads: {
      local authMiddlewareName = 'basic-auth-reads',
      basic_auth_secret: secret.new(authMiddlewareName, { users: std.base64(importstr 'secrets/basic-auth-read.secret.users.htpasswd') }),
      basic_auth: middleware.newBasicAuth(name=authMiddlewareName, secretName=authMiddlewareName, headerField='X-Scope-OrgID'),

      ingress: ingress.new(['mimir-read.tu.social'])
               + ingress.withMiddleware(authMiddlewareName)
               + ingress.withService('mimir-read', 8080)
               + ingress.withRoutePrefixService('mimir-backend', '/prometheus/config/v1/rules', port=8080)
               + ingress.withRoutePrefixService('mimir-backend', '/prometheus/api/v1/rules', port=8080)
               + ingress.withRoutePrefixService('mimir-backend', '/prometheus/api/v1/alerts', port=8080),
    },

    writes: {
      local authMiddlewareName = 'basic-auth-writes',
      basic_auth_secret: secret.new(authMiddlewareName, { users: std.base64(importstr 'secrets/basic-auth-write.secret.users.htpasswd') }),
      basic_auth: middleware.newBasicAuth(name=authMiddlewareName, secretName=authMiddlewareName, headerField='X-Scope-OrgID'),

      ingress: ingress.new(['mimir-write.tu.social'])
               + ingress.withMiddleware(authMiddlewareName)
               + ingress.withService('mimir-write', 8080),
    },

    admin: {
      local authMiddlewareName = 'basic-auth-admin',
      basic_auth_secret: secret.new(authMiddlewareName, { users: std.base64(importstr 'secrets/basic-auth-admin.secret.users.htpasswd') }),
      basic_auth: middleware.newBasicAuth(name=authMiddlewareName, secretName=authMiddlewareName, headerField='X-Scope-OrgID'),

      ingress: ingress.new(['mimir-admin.tu.social'])
               + ingress.withMiddleware(authMiddlewareName)
               + ingress.withService('mimir-backend', 8080),
    },

    default_tenant_admin: {
      local defaultTenantHeaderMiddleware = 'x-scope-orgid-default',
      defaultTenantHeader: middleware.new(name=defaultTenantHeaderMiddleware, spec={
        headers: {
          customRequestHeaders: {
            'X-Scope-OrgID': 'default',
          },
        },
      }),

      // This is an oauth-protected ingress that adds the X-Scope-OrgID header to the request.
      // Useful to access /alertmanager on it.
      ingress: ingress.new(['default.mimir-admin.tu.social'])
               + ingress.withMiddleware('oauth2-proxy', 'traefik')
               + ingress.withMiddleware(defaultTenantHeaderMiddleware, $._config.namespace)
               + ingress.withService('mimir-backend', 8080),
    },
  },
}
