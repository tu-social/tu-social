// Mimir Alertmanager configuration for the `default` tenant.
{
  _config:: {
    grafana_oncall_token:
      // This runs in CI on test phase, before git-crypt is unlocked, and we can't validate the config if it contains the encrypted secret.
      // So the script does --ext-str=DRY_RUN=$DRY_RUN
      if std.extVar('DRY_RUN') == 'true' then 'TOKEN'
      else importstr 'secrets/tusocial_grafana_net.oncall_integration_token.secret.txt',
  },

  route: {
    receiver: 'tusocial-grafana-oncall',
    group_by: [
      'alertname',
      'namespace',
      'cluster',
    ],
    routes: [
      {
        match: {
          heartbeat: 'true',
        },
        receiver: 'tusocial-grafana-oncall-heartbeat',
        group_wait: '0s',
        group_interval: '1m',
        repeat_interval: '50s',
      },
    ],
  },
  receivers: [
    {
      name: 'tusocial-grafana-oncall',
      webhook_configs: [
        {
          url: 'https://oncall-prod-eu-west-0.grafana.net/oncall/integrations/v1/alertmanager/%(grafana_oncall_token)s/' % $._config,
          send_resolved: true,
        },
      ],
    },
    {
      name: 'tusocial-grafana-oncall-heartbeat',
      webhook_configs: [
        {
          url: 'https://oncall-prod-eu-west-0.grafana.net/oncall/integrations/v1/alertmanager/%(grafana_oncall_token)s/heartbeat/' % $._config,
          send_resolved: false,
        },
      ],
    },
  ],
}
