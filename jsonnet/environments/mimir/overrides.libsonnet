{
  // TODO: maybe not mega user, but adding this as I need more rules.
  default+: self.mega_user {
    ruler_max_rules_per_rule_group: 50,
    compactor_blocks_retention_period: '1y',
  },
}
