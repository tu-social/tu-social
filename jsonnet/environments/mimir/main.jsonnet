local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local ingress = import 'ingress.libsonnet';
local mimir = import 'mimir/mimir.libsonnet';
local object_storage = import 'object_storage.libsonnet';
local ring = import 'ring.libsonnet';
local scaling = import 'scaling.libsonnet';

mimir + scaling + object_storage + ring + ingress {
  namespace: k.core.v1.namespace.new($._config.namespace),

  _config+:: {
    namespace: 'mimir',
    deployment_mode: 'read-write',

    multi_zone_ingester_enabled: true,
    multi_zone_store_gateway_enabled: true,

    mimir_write_replicas: 3,
    mimir_write_max_unavailable: 25,
    mimir_write_data_disk_size: '10Gi',
    mimir_write_data_disk_class: 'local-path',
    mimir_write_allow_multiple_replicas_on_same_node: true,
    mimir_read_replicas: 2,
    mimir_read_topology_spread_max_skew: 1,  // TODO: What's this?
    mimir_backend_replicas: 3,
    mimir_backend_max_unavailable: 10,
    mimir_backend_data_disk_size: '25Gi',
    mimir_backend_data_disk_class: 'local-path',
    mimir_backend_allow_multiple_replicas_on_same_node: true,

    // external_url is used for the alertmanager.
    external_url: 'https://default.mimir-admin.tu.social',

    overrides+: (import 'overrides.libsonnet'),
  },
}
