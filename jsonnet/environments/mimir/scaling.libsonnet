local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      deployment = k.apps.v1.deployment,
      statefulSet = k.apps.v1.statefulSet;
{
  local gomemlimit(limit) =
    assert std.endsWith(limit, 'B') : 'GOMEMLIMIT should env with B, like GiB, MiB, etc. contrary to Kubernetes requests/limits which can be Mi, Bi, etc.';
    k.core.v1.container.withEnvMixin([k.core.v1.envVar.new('GOMEMLIMIT', limit)]),

  mimir_read_container+::
    gomemlimit('512MiB')
    + k.util.resourcesRequests('100m', '512Mi')
    + k.util.resourcesLimits(null, '768Mi'),

  mimir_write_container+::
    gomemlimit('1GiB')
    + k.util.resourcesRequests('100m', '1Gi')
    + k.util.resourcesLimits(null, '1.5Gi'),

  mimir_backend_container+::
    gomemlimit('256MiB')
    + k.util.resourcesRequests('100m', '256Mi')
    + k.util.resourcesLimits(null, '512Mi'),

  local smallMemcached = {
    cpu_requests:: '100m',
    memory_limit_mb:: 64,
    memory_request_overhead_mb:: 8,
    statefulSet+: statefulSet.mixin.spec.withReplicas(1),
  },

  memcached_chunks+: smallMemcached,
  memcached_frontend+: smallMemcached,
  memcached_index_queries+: smallMemcached,
  memcached_metadata+: smallMemcached,
}
