local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      secret = k.core.v1.secret,
      container = k.core.v1.container,
      envVar = k.core.v1.envVar;

{
  _config+:: {
    storage_backend: 's3',
    storage_s3_access_key_id: '$(S3_ACCESS_KEY_ID)',
    storage_s3_secret_access_key: '$(S3_SECRET_ACCESS_KEY)',
    storage_s3_endpoint: 's3.fr-par.scw.cloud',

    alertmanager_storage_bucket_name: 'mimir-tu-social',
    blocks_storage_bucket_name: 'mimir-tu-social',
    ruler_storage_bucket_name: 'mimir-tu-social',

    storageConfig+:: {
      'alertmanager-storage.storage-prefix': 'alertmanager',
      'blocks-storage.storage-prefix': 'blocks',
      'ruler-storage.storage-prefix': 'ruler',
    },
  },

  mimir_object_storage_secret:
    secret.new('mimir-object-storage', {
      s3_access_key_id: std.base64(importstr 'secrets/s3-access-key-id.secret.txt'),
      s3_secret_access_key: std.base64(importstr 'secrets/s3-secret-access-key.secret.txt'),
    }, 'Opaque'),

  local containerEnvVarMixin = container.withEnvMixin([
    envVar.fromSecretRef('S3_ACCESS_KEY_ID', 'mimir-object-storage', 's3_access_key_id'),
    envVar.fromSecretRef('S3_SECRET_ACCESS_KEY', 'mimir-object-storage', 's3_secret_access_key'),
  ]),

  mimir_read_container+:: containerEnvVarMixin,
  mimir_write_container+:: containerEnvVarMixin,
  mimir_backend_container+:: containerEnvVarMixin,
}
