(import 'github.com/prometheus-community/postgres_exporter/postgres_mixin/mixin.libsonnet') {
  prometheusAlerts+:: {
    groups:
      // 'PostgreSQLCacheHitRatio' has no runbook, and it's triggered all the time by Mastodon.
      // Take the existing groups, and remove that alert from the rules.
      std.map(
        func=function(group)
          group {
            rules: std.filterMap(
              filter_func=function(rule) rule.alert != 'PostgreSQLCacheHitRatio',
              map_func=function(rule) rule,
              arr=group.rules,
            ),
          },
        arr=super.groups
      ) +
      [
        {
          name: 'postgres-single-primary',
          rules: [
            {
              alert: 'PostgresHasMultiplePrimaries',
              expr: 'count by (namespace, job) (count by (namespace, job, instance) (pg_replication_slots_active)) > 1',
              'for': '5m',
              labels: {
                severity: 'critical',
              },
              annotations: {
                description: "Check that there's only one primary per job.",
                summary: "There's more than one primary Postgres replica in {{ $labels.job }}.",
                runbook_url: 'https://gitlab.com/tu-social/tu-social/-/issues/13',
              },
            },
          ],
        },
      ],
  },
}
