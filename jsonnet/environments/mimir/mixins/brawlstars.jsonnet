{
  prometheusAlerts+:: {
    groups+: [
      {
        name: 'api',
        rules: [
          {
            alert: 'TooManyBrawlStarsAPIRequestsFailing',
            expr: |||
              # We ignore 5xx because BrawlStars is down too often, but we can't do anything about that. What we really care about are 4xx and `error` status codes.
              sum(increase(http_request_duration_seconds_count{job="brawlstars/brawlstars-exporter", status_code!~"2..|5.."}[10m])) > 10
            |||,
            'for': '2h',
            labels: {
              severity: 'critical',
            },
            annotations: {
              description: 'Check that not too many BrawlStars API requests are failing.',
              summary: '{{ $value }} BrawlStars API requests had non 2xx and non 5xx status code responses in the last 10m.',
            },
          },
          {
            alert: 'NoBrawlStarsAPIRequests',
            expr: |||
              absent(sum(increase(http_request_duration_seconds_count{job="brawlstars/brawlstars-exporter"}[10m])) > 0)
            |||,
            'for': '5m',
            labels: {
              severity: 'critical',
            },
            annotations: {
              description: 'Check that there are BrawlStars API requests.',
              summary: 'No BrawlStars API requests (with any outcome) in the last 10m.',
            },
          },
        ],
      },
    ],
  },
}
