(import 'github.com/prometheus/node_exporter/docs/node-mixin/mixin.libsonnet') {
  _config+:: {
    nodeExporterSelector: 'job="default/node-exporter"',
  },
}
