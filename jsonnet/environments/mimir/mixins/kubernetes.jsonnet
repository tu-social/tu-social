(import 'github.com/kubernetes-monitoring/kubernetes-mixin/mixin.libsonnet') {
  _config+:: {
    // Selectors are inserted between {} in Prometheus queries.
    cadvisorSelector: 'job="kube-system/cadvisor"',
    kubeletSelector: 'job="kube-system/kubelet"',
    kubeStateMetricsSelector: 'job="default/kube-state-metrics"',
    nodeExporterSelector: 'job="default/node-exporter"',
    podLabel: 'pod',
    hostNetworkInterfaceSelector: 'device!~"veth.+"',
    hostMountpointSelector: 'mountpoint="/"',
    containerfsSelector: 'container!=""',

    // TODO: https://github.com/k3s-io/k3s/issues/3619
    kubeSchedulerSelector: 'job="kube-scheduler"',
    kubeControllerManagerSelector: 'job="kube-controller-manager"',
    kubeApiserverSelector: 'job="kube-apiserver"',
    kubeProxySelector: 'job="kube-proxy"',
  },
  // We have kubernetes-mixin Prometheus alerts running in Grafana Cloud,
  // so don't provision them to avoid being double paged.
  prometheusAlerts: {},
}
