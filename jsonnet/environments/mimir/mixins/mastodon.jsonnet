{
  prometheusAlerts+:: {
    groups+: [
      {
        name: 'sidekiq',
        rules: [
          {
            alert: 'TooManyFailingSidekiqWorkerJobs',
            expr: |||
              (
                  sum by (worker) (increase(mastodon_sidekiq_worker_failure_total{worker!~"%(ignored)s"}[%(window)s]))
                  /
                  (
                      sum by (worker) (increase(mastodon_sidekiq_worker_failure_total{worker!~"%(ignored)s"}[%(window)s]))
                      +
                      sum by (worker) (increase(mastodon_sidekiq_worker_success_total{worker!~"%(ignored)s"}[%(window)s]))
                  )
              ) > 0.5
            ||| % {
              ignored: std.join('|', [
                // These will try to re-download already failed requests as timeouts, ssl, etc.
                // They're very likely to fail again, and sometimes they don't succeed for entire days.
                // We want to alert on primary workers, not on these.
                'RedownloadHeaderWorker',
                'RedownloadMediaWorker',
                'RedownloadAvatarWorker',
                'Web..PushNotificationWorker',
                'ActivityPub..LowPriorityDeliveryWorker',
              ]),
              // We use a 3h window because our instance isn't very busy so sometimes a single unreachable post can be 100% of failing requests in 30m.
              window: '3h',
            },
            'for': '5m',
            labels: {
              severity: 'critical',
            },
            annotations: {
              description: 'Check which portion of Mastodon Sidekiq jobs is failing for a given worker name.',
              summary: '{{ $value | humanizePercentage }}  Mastodon Sidekiq jobs for worker {{ $labels.worker }} failed during the last 6h in {{ $labels.namespace }}.',
            },
          },
          {
            alert: 'TooManyDyingSidekiqJobs',
            expr: |||
              sum by (namespace) (increase(mastodon_sidekiq_dead_size[1h]))
              /
              sum by (namespace) (increase(mastodon_sidekiq_processed[1h]))
              >
              0.025
            |||,
            'for': '5m',
            labels: {
              severity: 'critical',
            },
            annotations: {
              description: 'Check how many Mastodon Sidekiq jobs are dying.',
              summary: '{{ printf `sum(increase(mastodon_sidekiq_dead_size{namespace="%s"}[1h]))` $labels.namespace | query | value }}  Mastodon Sidekiq jobs died during the last hour in {{ $labels.namespace }}.',
            },
          },
        ],
      },
    ],
  },
}
