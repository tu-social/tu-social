{
  prometheusAlerts+:: {
    groups+: [
      {
        name: 'meta',
        rules: [
          {
            alert: 'GrafanaOnCallHeartbeat',
            expr: 'vector(1)',
            labels: {
              severity: 'none',
              heartbeat: 'true',
            },
            annotations: {
              description: 'This is a heartbeat alert for Grafana OnCall.',
              summary: 'Everything is fine.',
            },
          },
        ],
      },
    ],
  },
}
