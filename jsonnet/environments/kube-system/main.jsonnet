local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      configMap = k.core.v1.configMap,
      secret = k.core.v1.secret,
      container = k.core.v1.container,
      envVar = k.core.v1.envVar,
      storageClass = k.storage.v1.storageClass;

local csi_s3 = import 'k8s-csi-s3/csi.libsonnet';

{
  _config+:: {
    namespace: 'kube-system',
  },

  namespace: k.core.v1.namespace.new($._config.namespace),

  csi_s3: csi_s3 {
    _config+:: {
      namespace: $._config.namespace,
    },
  },

  csi_s3_secret:
    secret.new('csi-s3', {
      accessKeyID: std.base64(importstr 'secrets/csi-s3-access-key-id.secret.txt'),
      secretAccessKey: std.base64(importstr 'secrets/csi-s3-secret-access-key.secret.txt'),
      endpoint: std.base64('https://s3.fr-par.scw.cloud'),
      region: std.base64('fr-par'),
    }, 'Opaque'),

  scaleway_s3_retain_storageclass:
    storageClass.new('scaleway-s3-retain')
    + storageClass.withProvisioner('ru.yandex.s3.csi')
    + storageClass.withReclaimPolicy('Retain')
    + storageClass.withParameters({
      mounter: 'geesefs',
      bucket: 'tu-social-k8s-csi',
      options: '--memory-limit 1000 --dir-mode 0777 --file-mode 0666',
      'csi.storage.k8s.io/provisioner-secret-name': 'csi-s3',
      'csi.storage.k8s.io/provisioner-secret-namespace': 'kube-system',
      'csi.storage.k8s.io/controller-publish-secret-name': 'csi-s3',
      'csi.storage.k8s.io/controller-publish-secret-namespace': 'kube-system',
      'csi.storage.k8s.io/node-stage-secret-name': 'csi-s3',
      'csi.storage.k8s.io/node-stage-secret-namespace': 'kube-system',
      'csi.storage.k8s.io/node-publish-secret-name': 'csi-s3',
      'csi.storage.k8s.io/node-publish-secret-namespace': 'kube-system',
    }),

  scaleway_s3_dont_retain_storageclass:
    storageClass.new('scaleway-s3-dont-retain')
    + storageClass.withProvisioner('ru.yandex.s3.csi')
    + storageClass.withParameters({
      mounter: 'geesefs',
      bucket: 'tu-social-k8s-csi',
      options: '--memory-limit 1000 --dir-mode 0777 --file-mode 0666',
      'csi.storage.k8s.io/provisioner-secret-name': 'csi-s3',
      'csi.storage.k8s.io/provisioner-secret-namespace': 'kube-system',
      'csi.storage.k8s.io/controller-publish-secret-name': 'csi-s3',
      'csi.storage.k8s.io/controller-publish-secret-namespace': 'kube-system',
      'csi.storage.k8s.io/node-stage-secret-name': 'csi-s3',
      'csi.storage.k8s.io/node-stage-secret-namespace': 'kube-system',
      'csi.storage.k8s.io/node-publish-secret-name': 'csi-s3',
      'csi.storage.k8s.io/node-publish-secret-namespace': 'kube-system',
    }),

  coredns_custom_config: configMap.new('coredns-custom', {
    'disable-aaaa.override': importstr 'coredns/disable-aaaa.override',
  }),

}
