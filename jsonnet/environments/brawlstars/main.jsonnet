local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet',
      container = k.core.v1.container,
      containerPort = k.core.v1.containerPort,
      configMap = k.core.v1.configMap,
      deployment = k.apps.v1.deployment,
      envVar = k.core.v1.envVar,
      secret = k.core.v1.secret,
      service = k.core.v1.service,
      servicePort = k.core.v1.servicePort,
      statefulSet = k.apps.v1.statefulSet,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

local grafana = import 'grafana/grafana.libsonnet';
local prometheus = import 'prometheus/prometheus.libsonnet';
local ingress = import 'traefik/ingress.libsonnet';
local middleware = import 'traefik/middleware.libsonnet';
local postgres = import 'postgres/postgres.libsonnet';

{
  namespace: k.core.v1.namespace.new($._config.namespace),

  _config+:: {
    namespace: 'brawlstars',
    club_tag: '#2CQLU9QPC',
  },

  _images+:: {
    prometheus: 'prom/prometheus:v2.45.2',
    grafana: 'grafana/grafana:11.3.0',
  },

  brawlstars_exporter_secret:
    secret.new('brawlstars-exporter', {
      API_KEY: std.base64(importstr 'secrets/brawlstars-api-key.secret.txt'),
    }),

  brawlstars_exporter_container::
    container.new('brawlstars-exporter', 'registry.gitlab.com/tu-social/brawlstars-exporter:latest')
    + container.withImagePullPolicy('Always')
    + container.withEnvMixin([
      envVar.new('ALWAYS_UPDATE_PLAYERS', 'true'),
      envVar.new('UPDATE_INTERVAL', '1m'),
      envVar.new('CLUB_TAG', $._config.club_tag),
      envVar.fromSecretRef('API_KEY', 'brawlstars-exporter', 'API_KEY'),
    ])
    + container.withPorts([
      containerPort.new('http-metrics', 8080),
      containerPort.new('brawlstars', 8081),
    ])
    + k.util.resourcesRequests('20m', '64Mi'),

  brawlstars_exporter_deployment: deployment.new('brawlstars-exporter', 1, $.brawlstars_exporter_container),

  mastodon_service: service.new(
    'brawlstars-exporter',
    { name: 'brawlstars-exporter' },
    [servicePort.newNamed('brawlstars', 80, 'brawlstars') + servicePort.withProtocol('TCP')],
  ),

  default_mimir_writes_password_secret:
    secret.new('brawlstars-mimir-write', { 'brawlstars-mimir-write': std.base64(importstr 'secrets/mimir-write-brawlstars.secret.password.txt') }, 'Opaque'),

  prometheus: prometheus {
    _config+:: {
      name: 'prometheus-%s' % $._config.namespace,
      namespace: $._config.namespace,
      prometheus_requests_cpu: '100m',
      prometheus_requests_memory: '128Mi',
      prometheus_limits_cpu: null,
      prometheus_limits_memory: '256Mi',
    },
    prometheus_container+:: container.withVolumeMountsMixin([volumeMount.new('brawlstars-mimir-write', '/mimir-auth')]),
    prometheus_statefulset+: statefulSet.mixin.spec.template.spec.withVolumesMixin([volume.fromSecret('brawlstars-mimir-write', 'brawlstars-mimir-write')]),
    prometheus_config+:: {
      global: {
        scrape_interval: '60s',
      },
      remote_write: [
        {
          basic_auth: { username: 'brawlstars', password_file: '/mimir-auth/brawlstars-mimir-write' },
          url: 'https://mimir-write.tu.social/api/v1/push',
        },
      ],
    },
    scrape_configs:: {
      'brawlstars-exporter': {
        job_name: 'brawlstars-exporter',
        metrics_path: '/metrics',
        static_configs: [{ targets: ['brawlstars-exporter'] }],
      },
    },
    prometheus_pvc+::
      k.core.v1.persistentVolumeClaim.mixin.spec.resources.withRequests({ storage: '512Mi' }),
  },

  grafana_ingress: ingress.new(['brawlstars.tu.social'])
                   + ingress.withService('grafana'),

  mimir_datasource:: grafana.datasource.new('brawlstars @ mimir-read.tu.social', 'https://mimir-read.tu.social/prometheus', type='prometheus', default=true)
                     + grafana.datasource.withBasicAuth('brawlstars', importstr 'secrets/mimir-read-brawlstars.secret.password.txt'),

  grafana_admin_password_secret:
    k.core.v1.secret.new('grafana-admin-password', {
      password: std.base64(importstr 'secrets/grafana-admin.secret.password.txt'),
    }),

  grafana:
    grafana {
      _images+: $._images,
      grafana_container+::
        container.withEnvMixin([
          k.core.v1.envVar.fromSecretRef('ADMIN_PASSWORD', 'grafana-admin-password', 'password'),
          k.core.v1.envVar.new('GF_INSTALL_PLUGINS', 'dalvany-image-panel'),  // There's probably a mixin for this.
        ])
        + container.withVolumeMounts([
          volumeMount.new('postgres-secrets', '/etc/secrets/postgresql_password') + volumeMount.withSubPath('postgresql_password'),
        ]),
      grafana_deployment+:
        deployment.spec.template.spec.withVolumesMixin([
          volume.fromSecret('postgres-secrets', secretName='postgres'),
        ]),
    }
    + grafana.withGrafanaIniConfig({
      sections+: {
        'auth.anonymous': {
          enabled: true,
          org_role: 'Viewer',
          org_name: 'Tu Social',
          hide_version: true,
        },
        users: {
          default_theme: 'light',
          viewers_can_edit: true,
          home_page: '/d/ffa65891-1704-4eb2-8295-08c0b4bbe3d0/club-tu-social-de-brawlstars?kiosk',
        },
        security: {
          admin_password: '${ADMIN_PASSWORD}',
        },
        alerting: {
          enabled: false,
        },
        unified_alerting: {
          enabled: false,
        },
        database: {
          type: 'postgres',
          host: 'pgpool:5432',
          name: 'grafana',
          user: 'postgres',
          password: '$__file{/etc/secrets/postgresql_password}',
        },
        panels: {
          disable_sanitize_html: true,
        },
      },
    })
    + grafana.addDatasource('mimir-read.tu.social', $.mimir_datasource)
    + grafana.withRootUrl('https://brawlstars.tu.social'),

  postgres+: postgres {
    _images+:: $._images,

    _config+:: {
      pgpool_replicas: 2,
      postgres_replicas: 1,

      postgres_secret_name: 'postgres',
    },

    postgres_secret:
      secret.new('postgres', {
        postgresql_password: std.base64(importstr 'secrets/postgres-password.secret.txt'),
        repmgr_password: std.base64(importstr 'secrets/repmgr-password.secret.txt'),
        pgpool_admin_password: std.base64(importstr 'secrets/pgpool-admin-password.secret.txt'),
      }, 'Opaque'),
  },
}
