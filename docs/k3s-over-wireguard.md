# k3s over wireguard setup

- Follow the docs in https://docs.k3s.io/installation/network-options#distributed-hybrid-or-multicloud-cluster
- Check `ip addr` on both nodes
- [Following the advice](https://github.com/k3s-io/k3s/issues/7355#issuecomment-1523635066) set the `--node-ip` to the ip of `cni0` on each node. Remove `--node-external-ip`

The result looks like this:
```
$ kubectl get nodes -o wide
NAME    STATUS   ROLES                  AGE     VERSION        INTERNAL-IP   EXTERNAL-IP      OS-IMAGE                         KERNEL-VERSION    CONTAINER-RUNTIME
xeon    Ready    control-plane,master   471d    v1.25.4+k3s1   10.42.0.1     188.165.236.60   Debian GNU/Linux 11 (bullseye)   5.10.0-10-amd64   containerd://1.6.8-k3s1
zotac   Ready    <none>                 6m33s   v1.25.4+k3s1   10.42.1.1     <none>           Debian GNU/Linux 11 (bullseye)   5.10.0-22-amd64   containerd://1.6.8-k3s1
```