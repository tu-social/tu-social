# k3s upgrade command

```shell
curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" INSTALL_K3S_EXEC="server --disable=traefik --node-external-ip=188.165.236.60 --node-ip=10.42.0.1" sh -
```

Usually after each upgrade there's a new daemonset called `svclb-traefik` created in the `kube-system` namespace. Its pods won't start because ports are obviously already busy. It should be deleted to prevent the alerts from being triggered.
